# Setup

Install Python3

## Create local environment

`python -m venv derma_env`

Das env dann entweder über VsCode oder mit `derma_env/Scripts/activate` aktivieren

## Run mongodb instance

There are two options to build the mongo db instance:

1. Run in terminal: `docker run -d -p 27017:27017 --name mongo mongo`

2. Cd into ../docker and use: `docker-compose up -d --build` , then your database and backend container should be set up

## Set up backend

1. install requirements from requirements.txt (inside venv)

pip install -r requirements.txt

2. Run server

python main.py

_base url:_

http://localhost:8000

_Api docs_
http://localhost:8000/docs

3. If new dependencies, lock dependencies to requirements.txt

pip freeze -l > requirements.txt

# Docu

Referenz: https://www.mongodb.com/developer/languages/python/farm-stack-fastapi-react-mongodb/
