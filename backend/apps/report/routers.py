from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from .models import ReportModel, UpdateReportModel, ReportByTenantAndUserModel, ReportGroupByUser

router = APIRouter()


@router.post("/", response_description="Add new report")
async def create_report(request: Request, report: ReportModel = Body(...)):
    # encode report
    report = jsonable_encoder(report)
    # add to db
    new_report = await request.app.mongodb["report"].insert_one(report)
    # get created report
    created_report = await request.app.mongodb["report"].find_one(
        {"_id": new_report.inserted_id}
    )
    # return report
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_report)


@router.post("/filterByUserAndTenant", 
    description="Get all reports related to user and tenant",
    response_description="List of reports"
)
async def filter_by_tenant_and_user(request: Request, report: ReportByTenantAndUserModel = Body(...)):
    # encode report
    data = jsonable_encoder(report)
    
    # filter reports
    reports = []
    for item in await request.app.mongodb["report"].find({
        "user_uid": data["user_uid"],
        "tenant_uid": data["tenant_uid"],
    }).to_list(length=100):
        reports.append(item)

    # return report
    return reports


@router.get("/", response_description="List all reports")
async def list_reports(request: Request):
    reports = []
    # iterate all reports
    for doc in await request.app.mongodb["report"].find().to_list(length=100):
        # append report to array
        reports.append(doc)
    # return reports
    return reports


@router.post("/reportsGroupByUser", 
    description="Get all reports related to state and tenant and group by user",
    response_description="List of reports"
)
async def reports_group_by_user(request: Request, report: ReportGroupByUser = Body(...)):
    # encode report
    data = jsonable_encoder(report)
    
    reports_by_user = {}
    
    if data["type"] == "open" or data["type"] == "all":
        for doc in await request.app.mongodb["report"].find({
            "state": "waiting",
            "tenant_uid": data["tenant_uid"]
        }).to_list(length=100):
            # instantiate array for user
            if doc['user_uid'] not in reports_by_user:
                reports_by_user[doc['user_uid']] = []

            # append report to user in dictionary
            reports_by_user[doc['user_uid']].append(doc)

    if data["type"] == "closed" or data["type"] == "all":
        for doc in await request.app.mongodb["report"].find({
            "state": "available",
            "tenant_uid": data["tenant_uid"]
        }).to_list(length=100):
            # instantiate array for user
            if doc['user_uid'] not in reports_by_user:
                reports_by_user[doc['user_uid']] = []

            # append report to user in dictionary
            reports_by_user[doc['user_uid']].append(doc)

    return reports_by_user


@router.get("/{id}", response_description="Get a single report")
async def show_report(id: str, request: Request):
    if (report := await request.app.mongodb["report"].find_one({"_id": id})) is not None:
        # return found item
        return report
    # exception if there is no item
    raise HTTPException(status_code=404, detail=f"Task {id} not found")


@router.put("/{id}", response_description="Update a report")
async def update_report(id: str, request: Request, report: UpdateReportModel = Body(...)):
    report = {k: v for k, v in report.dict().items() if v is not None}

    if len(report) >= 1:
        # update item
        update_result = await request.app.mongodb["report"].update_one(
            {"_id": id}, {"$set": report}
        )
        # return updated report
        if update_result.modified_count == 1:
            if (
                updated_report := await request.app.mongodb["report"].find_one({"_id": id})
            ) is not None:
                return updated_report

    if (
        existing_report := await request.app.mongodb["report"].find_one({"_id": id})
    ) is not None:
        return existing_report
    
    # not found exception
    raise HTTPException(status_code=404, detail=f"Report {id} not found")


@router.delete("/{id}", response_description="Delete Report")
async def delete_report(id: str, request: Request):
    # delete item
    delete_result = await request.app.mongodb["report"].delete_one({"_id": id})

    # check if one ite got deleted
    if delete_result.deleted_count == 1:
        return {"ok": True}

    # not found exception
    raise HTTPException(status_code=404, detail=f"Report {id} not found")
