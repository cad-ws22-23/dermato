from typing import Optional
import uuid
from pydantic import BaseModel, Field


class ReportModel(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    name: str = Field(...)
    user_uid: str = Field(...)
    user_email: str = Field(...)
    tenant_uid: str = Field(...)
    state: str = Field(...)
    pdf: str = Field(...)

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "id": "000343203-0405-0607-0809-0a0b0c0d0e0f",
                "name": "Report 1",
                "user_uid": "few43r-0405-0607-0809-gtg3443",
                "user_email": "john@doe.com",
                "tenant_uid": "fd323-05-0607-0809-gtg3443",
                "state": "open",
                "pdf": "",
            }
        }


class UpdateReportModel(BaseModel):
    name: Optional[str]
    user_uid: Optional[str]
    user_email: Optional[str]
    tenant_uid: Optional[str]
    state: Optional[str]
    pdf: Optional[str]

    class Config:
        schema_extra = {
            "example": {
                "name": "Report 1",
                "user_uid": "few43r-0405-0607-0809-gtg3443",
                "user_email": "john@doe.com",
                "tenant_uid": "fd323-05-0607-0809-gtg3443",
                "state": "open",
                "pdf": "",
            }
        }


class ReportByTenantAndUserModel(BaseModel):
    user_uid: str
    tenant_uid: str
    
    class Config:
        schema_extra = {
            "example": {
                "user_uid": "few43r-0405-0607-0809-gtg3443",
                "tenant_uid": "fd323-05-0607-0809-gtg3443",
            }
        }


class ReportGroupByUser(BaseModel):
    type: str
    tenant_uid: str
    
    class Config:
        schema_extra = {
            "example": {
                "type": "open",
                "tenant_uid": "fd323-05-0607-0809-gtg3443",
            }
        }
