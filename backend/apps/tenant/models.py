from typing import Optional, Sequence
import uuid
from pydantic import BaseModel, Field, Extra


class TenantModel(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    fa_uid: str = Field(...)
    name: str = Field(...)
    license: str = Field(...)
    logo: str = Field(...)

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "fa_uid": "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
                "name": "DRK",
                "license": "freemium",
                "logo": "",
            }
        }


class UpdateTenantModel(BaseModel):
    fa_uid: Optional[str]
    name: Optional[str]
    license: Optional[str]
    logo: Optional[str]
