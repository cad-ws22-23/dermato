
from fastapi import APIRouter, Body, Depends, Request, HTTPException, status, File, UploadFile, Depends
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
import aiofiles
import logging
from bson import Binary, Code, json_util
from bson.json_util import loads
import json
import uuid

from .models import TenantModel, UpdateTenantModel
import os
from google.cloud import storage

# logging init
logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

# google cloud bucket init
bucket_name = 'dermato_find_app_bucket'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'dermatofind-service-key.json'
storage_client = storage.Client()

router = APIRouter()

def parse_json(data):
    return json.loads(json_util.dumps(data))


@router.post("/", response_description="Add new tenant")
async def create_tenant(request: Request, report: TenantModel = Body(...)):
    # encode tenant
    report = jsonable_encoder(report)
    # add to db
    new_report = await request.app.mongodb["tenant"].insert_one(report)
    # get created report
    created_report = await request.app.mongodb["tenant"].find_one(
        {"_id": new_report.inserted_id}
    )
    # return report
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_report)


@router.post("/createWithFile", response_description="Add new tenant entry", response_model=TenantModel)
async def create_tenant(request: Request, tenantItem = Depends(), file = File(...)):
    try:
        dir = "./files"
        async with aiofiles.open(f'{dir}/{file.filename}', 'wb') as f:
            contents = await file.read()
            await f.write(contents)
    except Exception:
        return {"message": "There was an error uploading the file"}
    finally:
        file_path = f'{dir}/{file.filename}'
        logger.info(file_path)
        # upload to gcloud bucket
        storageFile = 'https://storage.cloud.google.com/' + bucket_name + '/' + file.filename
        upload_to_bucket(file.filename, file_path, bucket_name)
        await file.close()
    
    form = await request.form()
    tenantObj = jsonable_encoder(form)

    tenant = jsonable_encoder(tenantItem)
    tenant["_id"] = str(uuid.uuid4())
    tenant["fa_uid"] = tenantObj["fa_uid"]
    tenant["name"] = tenantObj["name"]
    tenant["license"] = tenantObj["license"]
    tenant["logo"] = storageFile

    new_tenant = await request.app.mongodb["tenant"].insert_one(tenant)
    created_tenant = await request.app.mongodb["tenant"].find_one(
        {"_id": new_tenant.inserted_id}
    )
    created_tenant =  parse_json(created_tenant)

    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_tenant)


@router.get("/", response_description="List all tenant entries")
async def list_tenant_entries(request: Request):
    entries = []
    for doc in await request.app.mongodb["tenant"].find().to_list(length=100):
        entries.append(doc)
    parsed_entries = []
    for item in entries:
        parsed_entries.append(parse_json(item))
    return parsed_entries


@router.get("/{id}", response_description="Get a single tenant entry")
async def show_tenant_entry(id: str, request: Request):
    if (entry := await request.app.mongodb["tenant"].find_one({"_id": id})) is not None:
        return entry

    return None


@router.get("/fa/{id}", response_description="Get a single tenant entry")
async def show_tenant_entry(id: str, request: Request):
    if (entry := await request.app.mongodb["tenant"].find_one({"fa_uid": id})) is not None:
        return entry

    return None


@router.put("/{id}", response_description="Update a tenant entry")
async def update_tenant_entry(id: str, request: Request, entry: UpdateTenantModel = Body(...)):
    entry = {k: v for k, v in entry.dict().items() if v is not None}

    if len(entry) >= 1:
        update_result = await request.app.mongodb["tenant"].update_one(
            {"_id": id}, {"$set": entry}
        )

        if update_result.modified_count == 1:
            if (
                updated_entry := await request.app.mongodb["tenant"].find_one({"_id": id})
            ) is not None:
                return updated_entry

    if (
        existing_entry := await request.app.mongodb["tenant"].find_one({"_id": id})
    ) is not None:
        return existing_entry

    raise HTTPException(status_code=404, detail=f"Entry {id} not found")


@router.put("/logo/{id}", response_description="Update a tenant logo")
async def update_tenant_logo(id: str, request: Request, file = File(...)):
    try:
        dir = "./files"
        async with aiofiles.open(f'{dir}/{file.filename}', 'wb') as f:
            contents = await file.read()
            await f.write(contents)
    except Exception:
        return {"message": "There was an error uploading the file"}
    finally:
        file_path = f'{dir}/{file.filename}'
        logger.info(file_path)
        # upload to gcloud bucket
        storageFile = 'https://storage.cloud.google.com/' + bucket_name + '/' + file.filename
        upload_to_bucket(file.filename, file_path, bucket_name)
        await file.close()

    await request.app.mongodb["tenant"].update_one(
        {"_id": id}, {"$set": {"logo": storageFile}}
    )

    if (entry := await request.app.mongodb["tenant"].find_one({"_id": id})) is not None:
        return entry

    raise HTTPException(status_code=404, detail=f"Entry {id} not found")


@router.delete("/{id}", response_description="Delete tenant item")
async def delete_tenant_entry(id: str, request: Request):
    delete_result = await request.app.mongodb["tenant"].delete_one({"_id": id})
    logging.info(delete_result)

    if delete_result.deleted_count == 1:
        return {"message": "Deleted"}

    raise HTTPException(status_code=404, detail=f"Entry {id} not found")


# upload file
def upload_to_bucket(blob_name, file_path, bucket_name):
    try:
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return True
    except Exception as e:
        return False
