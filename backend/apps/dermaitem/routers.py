
from fastapi import APIRouter, Body, Depends, Request, HTTPException, status, File, UploadFile, Depends
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
import aiofiles
import logging
from bson import Binary, Code, json_util
from bson.json_util import loads
import json
import uuid

from .models import DermaItemModel, UpdateDermaItemModel, FindingsByReport
import os
from google.cloud import storage

# logging init
logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)

# google cloud bucket init
bucket_name = 'dermato_find_app_bucket'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'dermatofind-service-key.json'
storage_client = storage.Client()

router = APIRouter()

def parse_json(data):
    return json.loads(json_util.dumps(data))

@router.post("/upload")
async def upload(file: UploadFile = File(...)):
    logger.info(file)
    try:
        contents = await file.read()
        async with aiofiles.open(file.filename, 'wb') as f:
            await f.write(contents)
    except Exception:
        return {"message": "There was an error uploading the file"}
    finally:
        await file.close()

    return {"message": f"Successfuly uploaded {file.filename}"}


@router.post("/", response_description="Add new dermatology entry", response_model=DermaItemModel)
async def create_dermatologyItem(request: Request, dermaItem = Depends(), file = File(...)):
    try:
        dir = "./files"
        async with aiofiles.open(f'{dir}/{file.filename}', 'wb') as f:
            contents = await file.read()
            await f.write(contents)
    except Exception:
        return {"message": "There was an error uploading the file"}
    finally:
        file_path = f'{dir}/{file.filename}'
        logger.info(file_path)
        # upload to gcloud bucket
        storageFile = 'https://storage.cloud.google.com/' + bucket_name + '/' + file.filename
        upload_to_bucket(file.filename, file_path, bucket_name)
        await file.close()
    
    form = await request.form()
    dermaObj = jsonable_encoder(form)

    derma = jsonable_encoder(dermaItem)
    derma["_id"] = str(uuid.uuid4())
    derma["name"] = dermaObj["name"]
    derma["description"] = dermaObj["description"]
    derma["report_uid"] = dermaObj["report_uid"]
    derma["isMalign"] = (dermaObj["isMalign"] == 'true')
    derma["examination"] = dermaObj["examination"]
    derma["file"] = storageFile

    new_derma = await request.app.mongodb["derma"].insert_one(derma)
    created_derma = await request.app.mongodb["derma"].find_one(
        {"_id": new_derma.inserted_id}
    )
    created_derma =  parse_json(created_derma)

    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_derma)


@router.get("/", response_description="List all dermatology entries")
async def list_derma_entrys(request: Request):
    entries = []
    for doc in await request.app.mongodb["derma"].find().to_list(length=100):
        entries.append(doc)
    parsed_entries = []
    for item in entries:
        parsed_entries.append(parse_json(item))
    return parsed_entries


@router.post("/findingsByReport",
    description="List all findings from a report",
    response_description="Findings for report",
)
async def list_findings_by_report(request: Request, body: FindingsByReport = Body(...)):
    data = jsonable_encoder(body)
    findings = []
    for item in await request.app.mongodb["derma"].find({
        "report_uid": data["report_uid"],
    }).to_list(length=100):
        findings.append(item)
    return findings


@router.get("/{id}", response_description="Get a single dermatology entry")
async def show_dermatology_entry(id: str, request: Request):
    if (entry := await request.app.mongodb["derma"].find_one({"_id": id})) is not None:
        return entry

    raise HTTPException(status_code=404, detail=f"Entry {id} not found")


@router.put("/{id}", response_description="Update a dermatology entry")
async def update_dermatology_entry(id: str, request: Request, entry: UpdateDermaItemModel = Body(...)):
    entry = {k: v for k, v in entry.dict().items() if v is not None}

    if len(entry) >= 1:
        update_result = await request.app.mongodb["derma"].update_one(
            {"_id": id}, {"$set": entry}
        )

        if update_result.modified_count == 1:
            if (
                updated_entry := await request.app.mongodb["derma"].find_one({"_id": id})
            ) is not None:
                return updated_entry

    if (
        existing_entry := await request.app.mongodb["derma"].find_one({"_id": id})
    ) is not None:
        return existing_entry

    raise HTTPException(status_code=404, detail=f"Entry {id} not found")


@router.delete("/{id}", response_description="Delete dermatology item")
async def delete_derma_entry(id: str, request: Request):
    delete_result = await request.app.mongodb["derma"].delete_one({"_id": id})
    logging.info(delete_result)

    if delete_result.deleted_count == 1:
        return {"message": "Deleted"}

    raise HTTPException(status_code=404, detail=f"Entry {id} not found")


# upload file
def upload_to_bucket(blob_name, file_path, bucket_name):
    try:
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return True
    except Exception as e:
        return False
