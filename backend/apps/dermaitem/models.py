from typing import Optional, Sequence
import uuid
from pydantic import BaseModel, Field, Extra
from fastapi import Form, FastAPI, Depends
from dataclasses import dataclass


@dataclass
class ItemWithFile:
    _id: str = uuid.uuid4()
    name: str = Form(...)
    description: str = Form(...)
    tags: Optional[Sequence[str]] = Form(...)
    isMalign: bool = False
    examination: Optional[str] = Form(...)
    file: Optional[str] = ""



class DermaItemModel(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    name: str = Field(...)
    description: str = Field(...)
    report_uid: str = Field(...)
    isMalign: bool = False
    examination: Optional[str]
    file: Optional[str]

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "name": "My important entry",
                "description": "Test description",
                "isMalign": True,
                "examination": "Dr. House",
            }
        }


class UpdateDermaItemModel(BaseModel):
    name: Optional[str]
    description: Optional[str]
    isMalign: Optional[bool]
    examination: Optional[str]

    # class Config:
    #     schema_extra = {
    #         "example": {
    #             "name": "My important task",
    #             "completed": True,
    #         }
    #     }


class FindingsByReport(BaseModel):
    report_uid: str

    class Config:
        schema_extra = {
            "example": {
                "report_uid": "cc1924e2-749c-4e7a-9670-6698c3c15825",
            }
        }
