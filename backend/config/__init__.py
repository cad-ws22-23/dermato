from pydantic import BaseSettings


class CommonSettings(BaseSettings):
    APP_NAME: str = "derma find"
    DEBUG_MODE: bool = True


class ServerSettings(BaseSettings):
    HOST: str = "localhost"
    PORT: int = 8080


class DatabaseSettings(BaseSettings):
    DB_URL: str = "mongodb+srv://admin:admin@derma.yj9ejeq.mongodb.net/?retryWrites=true&w=majority"
    DB_NAME: str = "derma"


class Settings(CommonSettings, ServerSettings, DatabaseSettings):
    pass


settings = Settings()
