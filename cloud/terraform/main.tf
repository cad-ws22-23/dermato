terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file("dermatofind-service-key.json")

  project = "dermatofind-367420"
  region  = "europe-west3"
  zone    = "europe-west3-a"
}

resource "google_cloud_run_service" "frontend" {
  name     = "tf-frontend"
  location = "europe-west1"

  template {
    spec {
      containers {
        image = "europe-west1-docker.pkg.dev/dermatofind-367420/cloud-run-source-deploy/frontend@sha256:ea54f535e21af6e55ab61f5fb2323a88772e91468f7553e90d2504ae9670fada"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.frontend.location
  project     = google_cloud_run_service.frontend.project
  service     = google_cloud_run_service.frontend.name

  policy_data = data.google_iam_policy.noauth.policy_data
}

resource "google_cloud_run_service" "backend" {
  name     = "tf-backend"
  location = "europe-west1"

  template {
    spec {
      containers {
        image = "europe-west1-docker.pkg.dev/dermatofind-367420/cloud-run-source-deploy/backend@sha256:fadfae3bfc6834231c3dc4e70a894f0225fc1d98be84685795c12e29e321ef42"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth-backend" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth-backend" {
  location    = google_cloud_run_service.backend.location
  project     = google_cloud_run_service.backend.project
  service     = google_cloud_run_service.backend.name

  policy_data = data.google_iam_policy.noauth-backend.policy_data
}

resource "google_storage_bucket" "tf-dermato_find_app_bucket" {
  name          = "tf-dermato_find_app_bucket"
  location      = "europe-west1"
  force_destroy = true
  
  cors {
    origin          = ["*"]
    method          = ["GET", "HEAD", "PUT", "POST", "DELETE"]
    response_header = ["*"]
    max_age_seconds = 3600
  }
}