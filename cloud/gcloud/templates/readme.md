# Deploy whole application to gookle kubernetes engine (gke)

When you already have the docker container images in your google artifact registry then
skip point 1. and continue with 2.

## 1. Upload Container images (skip if images already uploaded)

//Select right project
(make sure project exits or create it)
gcloud config set project {projectname}

//Create Repository
GCloud Console > Artifact Registry > Reporsitory erstellen

- Name: dermato
- Region: europe-west3
  --> Create repository

//Connect / authorize cloud
gcloud auth configure-docker europe-west3-docker.pkg.dev

//upload auth helper
cd path/to/project
docker build -t dermato-auth-helper:1.0.0 .
docker tag dermato-auth-helper:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-auth-helper:1.0.0
docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-auth-helper:1.0.0

//upload auth helper image
cd path/to/project/auth/auth-server
docker build -t dermato-auth-helper:1.0.0 .
docker tag dermato-auth-helper:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-auth-helper:1.0.0
docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-auth-helper:1.0.0

//upload backend image
cd path/to/project/backend
docker build -t dermato-be:1.0.0 .
docker tag dermato-be:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-be:1.0.0
docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-be:1.0.0

//upload frontend image
cd path/to/project/frontend
docker build -t dermato-fe:1.0.0 .
docker tag dermato-fe:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-fe:1.0.0
docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-fe:1.0.0

//upload pdf image
cd path/to/project/pdf
docker build -t dermato-pdf:1.0.0 .
docker tag dermato-pdf:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-pdf:1.0.0
docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-pdf:1.0.0

## 2. Create cluster

//create cluster
gcloud container clusters create dermato-cluster --num-nodes=1 --zone=europe-west3-a

//tip: in gcloud console click on cluster and on "connect" > copy string (which is line below) -- update to your project id
gcloud container clusters get-credentials dermato-cluster --zone europe-west3-a --project dermato-372113

## 3. Deploy application

//deploy all deployments/services/configmaps
cd /path/to/project/cloud/gcloud/templates/
kubectl apply -R -f .

## 4. Update configs - use current ips

//get ips
kubectl get ingress

//edit file 02-fusion-auth/03-dermato-fusion-auth-configmap.yaml
(manually) update variable: frontendIp
kubectl apply -f 02-fusion-auth/03-dermato-fusion-auth-configmap.yaml

//edit file 03-dermato-auth-helper/07-dermato-auth-helper-configmap.yaml
(manually) update variables: frontendIp, fusionAuthIp
kubectl apply -f 03-dermato-auth-helper/07-dermato-auth-helper-configmap.yaml

//rollout new deployments (restarts the pods with the new configs)
kubectl rollout restart deployment/dermato-auth-helper
kubectl rollout restart deployment/dermato-fusion-auth

now the application is ready - go to the frontend ingress ip
log in with a user configured in the 03-dermato-fusion-auth-configmap.yaml file
