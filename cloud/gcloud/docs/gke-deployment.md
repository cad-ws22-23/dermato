# create deployment for microservice in k8s

kubectl create deployment dermato-fe-deploy --image=europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-fe@sha256:37c78d3eedbba9960946238c740aace3a1d88ae6d10208b6f72b8b32c6129a23

# better create via yaml config file

kubectl apply -f <filenname>

# delete

kubectl delete deployment hello-world
