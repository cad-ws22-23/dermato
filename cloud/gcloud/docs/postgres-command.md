# Postgres commands

## login

su postgres (switch current user to the postgres user you setted up)
psql

or

psql -U postgres

## list users

\du
\du+ (more info)

## create user

CREATE USER <name> WITH PASSWORD '<password>';
create user fusionauth with password 'mufBMuiGHP0_vn9Q1r7UZey-3xg63CLUuWDvx5YrYWw';

## list all databases

\list
\l

## connect to certain database

\c <db name>

## list all tables in the current database (using your search path)

\dt

## list all tables in the current database (regardless your search path)

\dt \*.

## switch databases

\connect database_name
\c database_name

## create database

create database fusionauth;

## export database

pg_dump -U dbusername dbname > dbexport.pgsql

## import database

psql -U username dbname < dbexport.pgsql
psql -U postgres fusionauth < fa_db-20221229.pgsql

## delete database

drop database fusionauth;

## is ready healtch check

https://stackoverflow.com/questions/46516584/docker-check-if-postgres-is-ready

# check if docker container is ready

timeout 90s bash -c "until docker exec fusionauth_db_1 pg_isready ; do sleep 5 ; done"

# check if postgres pod in gke is available

timeout 90s bash -c "until kubectl exec << pod id >> pg_isready ; do sleep 5 ; done"
timeout 90s bash -c "until kubectl exec dermato-fusion-db-6dc86d44bb-4xzsz pg_isready ; do sleep 5 ; done"
