# Manually

## create cluster

gcloud container clusters create dermato-cluster --num-nodes=1 --zone=europe-west3-a

## tip: in gcloud console click on cluster and on connect > copy string

gcloud container clusters get-credentials dermato-cluster --zone europe-west3-a --project dermato-372113

## Delete

gcloud container clusters delete CLUSTER_NAME

# Via Terraform

## TODO
