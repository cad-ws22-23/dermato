# Config

## Select right project

(make sure project exits or create it)
gcloud config set project {projectname}

## Create repository

GCloud Console > Artifact Registry > Reporsitory erstellen

- Name: dermato
- Region: europe-west3
  --> Create repository

## Connect / authorize cloud

gcloud auth configure-docker europe-west3-docker.pkg.dev

## List artifacts

gcloud artifacts repositories list

# Frontend

## Creating local image from frontend

cd path/to/project/frontend
docker build -t dermato-fe:1.0.0 .

## Tag Docker frontend image

docker tag {source-image} {location}-docker.pkg.dev/{project-id}/{repository}/{image}
docker tag dermato-fe:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-fe:1.0.0

# Push Container

docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-fe:1.0.0

# Backend

## Creating local image from backend

cd path/to/project/backend
docker build -t dermato-be:1.0.0 .
docker tag dermato-be:1.0.0 europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-be:1.0.0
docker push europe-west3-docker.pkg.dev/dermato-372113/dermato/dermato-be:1.0.0

# Building image out of existing container

docker commit Webserver WebserverImage:v1.0
