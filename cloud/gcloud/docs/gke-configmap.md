# Info

ConfigMaps in Kubernetes are, as the name suggests, used for providing configurations in a key-value approach, e.g., environment variables for services and making them accessible for Deployments and other Resources.

But besides referencing the ConfigMaps and only extracting its values and pushing it into environment variables, ConfigMaps can also be mounted as a volume.

# deploy

kubectl apply -f <filename>

# updates

Please keep in mind that when you change the content of the ConfigMap and apply the new ConfigMap to your cluster, it won't affect your already running Pods.

The reason here is that the Pods mount the content of the ConfigMap once they start. Afterward, changes to the ConfigMap don't affect the Pods anymore.

If you don't want to make changes to your Deployment to trigger a restart of your pods to fetch the new ConfigMap content, which you absolutely shouldn't, you can easily restart the containers via the kubectl rollout command.

For example, to restart the Pod of the NGINX deployment from this tutorial, you can use:

kubectl rollout restart deployment/nginx

# reference

https://levelup.gitconnected.com/mount-files-into-your-kubernetes-deployment-via-configmaps-bedfe5350e25
