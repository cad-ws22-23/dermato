# list all services

kubectl get all

## List artifacts

gcloud artifacts repositories list

# deploy yml files

kubectl apply --filename ingress.yaml
kubectl apply -f ingress.yaml

# deploy multiple yml files

kubectl apply -f << folder >>

# see pod logs

kubectl logs << pod-id >>
kubectl logs pod/dermato-be-7ffccb97d6-f4jlj

# got into shell of pod

kubectl exec --stdin --tty << pod-id >> -- /bin/bash

or

kubectl exec -it << pod-id >> -- /bin/sh

# copy file from host into kubernetes pod

kubctl cp fa_db-20221229.pgsql dermato-fusion-db-6dc86d44bb-4xzsz:/path/to/copy

# port forwarding

port forwarding is very powerful for debugging, even if the connection is slow.
you can foward a pod sending traffic to a localhost port on your machine.
then you can reach the pod via localhost:9011 in this example

kubectl port-forward RESOURCE_TYPE/RESOURCE_NAME LOCAL_PORT/RESOURCE_PORT
kubectl port-forward dermato-fusion-auth-567b6f9fcd-266p2 9011:9011
