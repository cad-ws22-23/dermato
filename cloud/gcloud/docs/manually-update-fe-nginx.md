# Manually update nginx conf from frontend pod

## go into pod shell

kubectl exec -it << pod-id >> -- /bin/sh

## edit conf

...

## reload nginx

nginx -s reload
