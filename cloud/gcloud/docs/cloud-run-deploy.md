# Gcloud deploy

1. Neues Projekt in Google Cloud erstellen

2. Google Cloud Platform CLI installieren https://cloud.google.com/sdk/docs/install

3. Config durchgehen (mit google acc anmelden)

## Frontend

cd /path/to/project/frontend

gcloud config set project <projectname>

gcloud run deploy frontend --source .

## Backend

### Setup mongodb

Login to https://cloud.mongodb.com/ (zB mit google account)

Cluster erstellen network settings am einfachsten 0.0.0.0 whitelisten

Unter "Connect" den connection string im backend einfÃ¼gen

### Depploy backend to gcp

cd /path/to/project/backend

gcloud run deploy backend --source .
