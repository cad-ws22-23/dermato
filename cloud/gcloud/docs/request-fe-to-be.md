# Initial setup and problem

The first approach was that i created a backend pod via deployment and a related service.
The same did i for the frontend.
Addtionally i added one ingress which gives external access to the frontend.
In the frontend i defined "http://dermato-be:8080" as backend url. Since the request
is server side, means requested from the browser and so outside the cluster it fails.

# Solution

React is calling its on host which is inside the pod by using relative phats without host.
Then the Frontend Nginx is called.
Configure there a proxy_pass to the "http://dermato-be:8080" Domain, which represents the
backend pods inside the cluster

# Helpful references for this idea

https://amlanscloud.com/reactappdeploy/
https://amlanscloud.com/reactappdeploy2/
https://github.com/amlana21/deploy-react-kube/blob/master/codes/default.conf
https://github.com/amlana21/deploy-react-kube/blob/master/codes/contact-appv1/src/App.js
