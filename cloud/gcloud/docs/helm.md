see https://fusionauth.io/docs/v1/tech/installation-guide/kubernetes/fusionauth-deployment

# install config

helm install [CHART NAME] [CHART] [flags]
helm install my-release fusionauth/fusionauth -f values.yaml
