# get all ingresses

kubectl get ingress
or
gcloud compute forwarding-rules list --filter dermato-app-ingress --format "table[box](<name,IPAddress,target.segment(-2):label=TARGET_TYPE>)"

# delete

kubectl delete ingress basic-ingress
