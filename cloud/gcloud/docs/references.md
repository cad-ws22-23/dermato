# example of loadblanacing service vs ingress - blog

https://medium.com/google-cloud/deploying-service-or-ingress-on-gke-59a49b134e3b

docu depracted. update following configs

https://stackoverflow.com/questions/66236346/kubernetes-apiversion-networking-k8s-io-v1-issue-with-ingress

# gke cretae cluster with terraform - blog

https://joachim8675309.medium.com/building-gke-with-terraform-869df1cd3f41

# gke cretae cluster with terraform - video

https://www.youtube.com/watch?v=qf_fsOEj-OU

# Gke kubernetes tutorial, general aspects - video

https://www.youtube.com/watch?v=jW_-KZCjsm0
https://www.youtube.com/watch?v=s_o8dwzRlu4

# Gke docu for deploy / service / ingress

https://cloud.google.com/kubernetes-engine/docs/tutorials/http-balancer#remarks

# Official kubernets docu

https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

# Pod communication

https://www.tutorialworks.com/kubernetes-pod-communication/

# Docker image creating

https://www.heise.de/hintergrund/Windows-und-Linux-basierte-Docker-Container-auf-Windows-nutzen-Teil-2-von-2-3747615.html?seite=2

# Deploy multiple files

https://stackoverflow.com/questions/59491324/is-there-a-way-to-kubectl-apply-all-the-files-in-a-directory?rq=1
