# Install kube control

gcloud components install kubectl

# For upcoming new 1.26 version

gcloud components install gke-gcloud-auth-plugin
--> see here: https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke?hl=en
