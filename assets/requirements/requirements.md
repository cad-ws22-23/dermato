```
Seite | 1
```

Cloud Native Project - Cloud Application Development – Winter Term 2022 /2 3

Prof. Dr. Markus Eiglsperger

# Project

Individual project parallel to the lecture to realize a cloud native application:

- Each team (max. 4 members) specifies, designs, realises and validates a cloud application.
- Continuous work on the project is advised.

Deadlines:

```
Kickoff: 10. 11 .20 22
Specification: 17. 11. 2022
Implementation: 28 .0 1 .20 22
Presentation planned for 28 .0 1 .20 22
Documentation: 05 .0 2 .20 22
```

Technicial Requirements for the Application

- The application runs on standard cloud platform
- The application implements a multi-user, multi-tenant Software-as-a-Service (SaaS)
- The application implements the five essential characteristics of a cloud service:
  o On-demand self service
  o Broad network access
  o resource pooling
  o rapid elasticity
  o measured service
- The application architecture is cloud native and supports the 12 factors
- The application is implemented using micro-services,
- Some of the services have to be deployed to kubernetes
- The application provides a WebUI and an API
- The application contains on demand workloads (e.g. WEBUI interaction, API Calls) and long
  running asynchronous workloads (e.g. scheduled job to create report)

DevOps Requirements

- The cloud environment can be setup and updated automatically (Infrastructure as code)
- New releases of the application can be deployed using continuous delivery
- The application provides telemetry data for health and usage
- Identity and Access Management and Network security rules guarantee the security of the
  application and the processes

Commercial Requirements

- There is a cost calculation for the running costs of the application
- There is a commercial model for the services provided by the cloud

Documentation

Provide Documentation as a single PDF or provide a markdown file in the git repo. As a guideline for the
size of the documentation, you should provide the equivalent of 5- 10 pages text/graphic per team member.
As a rule of thumb, if you spent significant time to implement a certain feature or to solve a certain problem,
then it might well be worth to describe this in more detail in the documentation.
