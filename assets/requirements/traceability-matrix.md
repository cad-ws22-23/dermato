# Traceability matrix

The following matrix gives a overview of the relation of the single requirements to the chapters in the documentation, where the requirement
is fulfilled and the implementation and described

| Requirement                                                                                                                                                      | Chapter           |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| Technicial Requirements for the Application                                                                                                                      | see subcategories |
| • The application runs on standard cloud platform                                                                                                                | 4.1               |
| • The application implements a multi-user, multi-tenant Software-as-a-Service (SaaS)                                                                             | 3.3, 3.4, 4.2     |
| • The application implements the five essential characteristics of a cloud service                                                                               | 4.3               |
| • The application architecture is cloud native and supports the 12 factors                                                                                       | 4.4               |
| • The application is implemented using micro-services                                                                                                            | 4.6               |
| • Some of the services have to be deployed to kubernetes                                                                                                         | 4.6               |
| • The application provides a WebUI and an API                                                                                                                    | 4.7, 4.8          |
| • The application contains on demand workloads (e.g. WEBUI interaction, API Calls) and long running asynchronous workloads (e.g. scheduled job to create report) | 4.9               |
| -------------------------------------------                                                                                                                      | ----------------- |
| DevOps Requirements                                                                                                                                              | see subcategories |
| • The cloud environment can be setup and updated automatically (Infrastructure as code)                                                                          | 5.1, 5.2          |
| • New releases of the application can be deployed using continuous delivery                                                                                      | 5.2               |
| • The application provides telemetry data for health and usage                                                                                                   | 5.3               |
| • Identity and Access Management and Network security rules guarantee the security of the application and the processes                                          | 6                 |
| -------------------------------------------                                                                                                                      | ----------------- |
| Commercial Requirements                                                                                                                                          | see subcategories |
| • There is a cost calculation for the running costs of the application                                                                                           | 7.1               |
| • There is a commercial model for the services provided by the cloud                                                                                             | 7.2               |
