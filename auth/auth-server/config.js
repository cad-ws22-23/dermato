module.exports = {
  //api key valid for all applications
  apiKey: "6740dbfc-a1fb-46b3-8a67-2fa07f5075e3",

  //ports
  serverPort: 3011,

  //services
  dermato: "localhost:3000",
  fusionAuth: "localhost:9011",
  fusionAuthInternal: "host.docker.internal:9011",

  //tenant based infos
  tenant: [
    {
      id: "d420a168-7580-497e-9136-da5cacb0927e",
      applicationID: "d420a168-7580-497e-9136-da5cacb0927e",
      clientID: "d420a168-7580-497e-9136-da5cacb0927e",
      clientSecret: "lFJf7MCPk5uz2XcPt0s-484f0CbYOmJ1z3FdYqJoPmo",
      redirectURI:
        "http://localhost:3011/oauth-callback/d420a168-7580-497e-9136-da5cacb0927e",
    },
    {
      id: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      applicationID: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      clientID: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      clientSecret: "Py5EVhkzHsC2Rea7l5qgpgLO1_iegKQStcF0Ko7AB2E",
      redirectURI:
        "http://localhost:3011/oauth-callback/c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
    },
    {
      id: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      applicationID: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      clientID: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      clientSecret: "YO8sYM9JJK2Ur8j0xvFZkLy-1kJHuJg5fBknzi94n8I",
      redirectURI:
        "http://localhost:3011/oauth-callback/c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
    },
  ],
};
