const express = require("express");
const router = express.Router();
const config = require("../config");
const pkce = require("../helpers/pkce");

router.get("/", (req, res) => {
  // Generate and store the PKCE verifier
  req.session.verifier = pkce.generateVerifier();

  // Generate the PKCE challenge
  const challenge = pkce.generateChallenge(req.session.verifier);

  //get tenant conf
  const tenantConf = config.tenant.find(
    (tenant) => tenant.id === res.req.query["tenant"]
  );

  res.redirect(
    `http://${config.fusionAuth}/oauth2/authorize?client_id=${tenantConf.clientID}&redirect_uri=${tenantConf.redirectURI}&response_type=code&code_challenge=${challenge}&code_challenge_method=S256`
  );
});

module.exports = router;
