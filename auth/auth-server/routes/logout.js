const express = require("express");
const router = express.Router();
const config = require("../config");

router.get("/", (req, res) => {
  // delete the session
  req.session.destroy();

  //get tenant conf
  const tenantConf = config.tenant.find(
    (tenant) => tenant.id === res.req.query["tenant"]
  );

  // end FusionAuth session
  res.redirect(
    `http://${config.fusionAuth}/oauth2/logout?client_id=${config.tenant[0].clientID}`
  );
});

module.exports = router;
