const express = require("express");
const router = express.Router();
const request = require("request");
const config = require("../config");

router.get("/", (req, res) => {
  //get tenant conf
  const tenant_id = req.baseUrl.split("/oauth-callback/")[1];
  const tenantConf = config.tenant.find((tenant) => tenant.id === tenant_id);

  request(
    // POST request to /token endpoint
    {
      method: "POST",
      uri: `http://${config.fusionAuthInternal}/oauth2/token`,
      form: {
        client_id: tenantConf.clientID,
        client_secret: tenantConf.clientSecret,
        code: req.query.code,
        code_verifier: req.session.verifier,
        grant_type: "authorization_code",
        redirect_uri: tenantConf.redirectURI,
      },
    },

    // callback
    (error, response, body) => {
      if (error !== null) console.log("error:", error);
      else console.log("body:", body);

      // save token to session
      req.session.token = JSON.parse(body).access_token;

      // redirect to react app
      res.redirect(`http://${config.dermato}?tenant=${tenant_id}`);
    }
  );
});

module.exports = router;
