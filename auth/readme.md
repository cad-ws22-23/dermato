# Auth architecture

React (3000) <-> ExpressJS (3011) <-> Fusionauth (9011)

# Initialize fusion auth and database

cd to `path/to/project/auth/fusionauth`
`docker-compose up -d`

Fill in the form

Host: db
Port: 5432
Database: fusionauth

Superuser credentials
Username: postgres
Password: 9z439rfnfSdsfA!#edc"sqss

FusionAuth credentials
Username: fusionauth
Password: mufBMuiGHP0_vn9Q1r7UZey-3xg63CLUuWDvx5YrYWw

Administrator account
Choose your values

# Adding new Tenants in fusionauth

1. Go to tenants > new
2. Type in name and save new tenant
3. copy and note down tenant id
4. Go to applicatons > new
5. Type in name: `Dermato-<<tenant-id>>`
6. Select the right tenant
7. Add 2 roles in the roles tab:
   1. Name: Patient, Description: Patient
   2. Name: Doctor, Description: Doctor
   3. Name: Custom Description: Custom
8. Add Authorized redirect URLs in OAuth Tab like: `http://localhost:3011/oauth-callback/<<tenant-id>>`
9. Add Logout Link in OAuth Tab: `http://localhost:3000/`
10. Save Application
11. Click on show Application
12. Note down applicationID, clientID, clientSecret, redirectURI
13. Add this information in following form to the tenants array in the `/auth/config.js` file

`{
id: "d794664b-0234-454c-a634-c9ea4e9528e1",
applicationID: "8fbd738f-e676-4be3-a498-03c77e816d59",
clientID: "8fbd738f-e676-4be3-a498-03c77e816d59",
clientSecret: "y39aJVYwcyGOU2qGMs8CfeRONuKmPDDWSvtc1FIBvRc",
redirectURI:
"http://localhost:3011/oauth-callback/d794664b-0234-454c-a634-c9ea4e9528e1",
},`

# Adding new users in fusionauth

1. Go to users > new
2. Type in Email
3. Unselect "Send email to set up password"
4. Type in Password
5. Add registration
   1. Select Role
   2. Save

# Start auth server

`cd path/to/project/auth/auth-server`
`npm install`
`npm run serve`
