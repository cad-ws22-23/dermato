# importing modules
import pathlib
# from reportlab.platypus import Pagebreak
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import inch
import os
from google.cloud import storage
from bson import Binary, Code, json_util
from bson.json_util import loads
import json
import uuid
from io import BytesIO
from PIL import Image
from fpdf import FPDF
import requests


# google cloud bucket init
#os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'google-derma.json'
bucket_name = 'dermato_find_app_bucket'
storage_client = storage.Client()

class PDF(FPDF):
    def header(self):
        self.image("dermato-logo.png", 100, 10, 20)
        # Line break
        self.ln(20)

def removeLocalFile(filename):
    if os.path.exists(filename):
        os.remove(filename)
    else:
        print("The file does not exist")

# Reads iamge file from google cloud bucket
def read_file_blob(file_link):
    """Read a file from the bucket."""
    blob_name = file_link.rsplit('/', 1)[-1]
    bucket_name = file_link.rsplit('/', 1)[-2]
 
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)
 
    # read as string
    read_output = blob.download_as_string()
 
    print(
        "File {} read successfully  from Bucket  {}.".format(
            blob_name, bucket_name
        )
    )

def parse_json(data):
    return json.loads(json_util.dumps(data))


# upload file
def upload_to_bucket(blob_name, file_path, bucket_name):
    try:
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.blob(blob_name)
        created = blob.upload_from_filename(file_path)
        return True
    except Exception as e:
        print(e)
        return False

def addFindingToPDF(item, pdf, bucket):

    # Add a new page to the PDF
    pdf.add_page()

    # Download the image from the URL specified in the 'file' property of the item
    response = requests.get(item.file)
    fname = item.file.split("/")[-1]
    blob = bucket.blob(fname)
    tempImg = blob.download_to_filename("tempImg.jpg")
    malign = "Bösartig: Ja" if item.isMalign else "Bösartig: Nein"

    # Add the name, description, isMalign, and examination properties to the page
    pdf.set_xy(10, 20)
    pdf.set_font('Arial', 'B', 16)
    pdf.cell(0, 10, item.name)

    pdf.set_xy(10, 30)
    pdf.set_font('Arial', '', 12)
    pdf.cell(0, 10, f"Beschreibung: {item.description}")

    pdf.set_xy(10, 40)
    pdf.set_font('Arial', '', 12)
    pdf.cell(0, 10, "Untersuchungsbild:")
    pdf.image("tempImg.jpg", 10, 50, 60)

    pdf.set_xy(10, 130)
    pdf.set_font('Arial', '', 12)
    pdf.multi_cell(0, 10, f"Untersuchungsergebnis: {item.examination}")

    pdf.set_xy(10, 140)
    pdf.set_font('Arial', '', 12)
    pdf.cell(0, 10, malign)

    removeLocalFile("tempImg.jpg")


class ReportService():

    # Uploads Report to bucket
    # Parameters: filename (string)
    def uploadToCloud( filename):
        dir = "."
        file_path = f'{dir}/{filename}'
        storageFile = 'https://storage.cloud.google.com/' + bucket_name + '/' + filename
        try:
            created  = upload_to_bucket(filename, file_path, bucket_name)
            return storageFile
        except Exception as e:
            return e


    # Creates a new report
    # Parameters: report
    def create_pdf(report):
        fileName = f"{report.user}-{report.name}.pdf"
        # Create a new PDF document
        pdf = PDF()
        bucket = storage_client.get_bucket(bucket_name)

        # Iterate through the list of items
        for item in report.items:
            addFindingToPDF(item, pdf, bucket)


        # Save the PDF to a file
        pdf.output(fileName)
        return fileName
    
    def removeLocalPDF(filename):
        if os.path.exists(filename):
            os.remove(filename)
        else:
            print("The file does not exist")

