from typing import Optional, List
import uuid
from pydantic import BaseModel, Field


class Finding(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    name: str = Field(...)
    description: str = Field(...)
    isMalign: bool = False
    examination: str
    report_uid: str
    file: str

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                    "id": "edde014c-6b0f-4552-80f5-f3c8d065c7d3",
                    "name": "Leberfleck",
                    "description": "Leberfleck an der linken Schulter",
                    "report_uid": "f71adb87-4618-4e51-b9be-ad2976286cea",
                    "isMalign": False,
                    "examination": "Der Leberfleck sieht nicht weiter gefährlich aus",
                    "file": "https://storage.cloud.google.com/dermato_find_app_bucket/finding-2.jpg"
               
            }
        }


class ReportModel(BaseModel):
    user: str
    name: str
    items: List[Finding]

    class Config:
        allow_population_by_field_name = True
        schema_extra = {
            "example": {
                "user": "1232safasd2ad2adsa",
                "name": "reprotname",
                "items": [
                {
                    "id": "adde014c-6b0f-4552-80f5-f3c8d065c7d3",
                    "name": "Leberfleck1",
                    "description": "Leberfleck an der linken Schultersad",
                    "report_uid": "f71adb87-4618-4e51-b9be-ad2976286cea",
                    "isMalign": False,
                    "examination": "Der Leberfleck sieht nicht weiter gefährlich aus",
                    "file": "https://storage.cloud.google.com/derma_pdfgen/finding-2.jpg"
                },
                {
                    "id": "edde014c-6b0f-4552-80f5-f3c8d065c7d3",
                    "name": "Leberfleck",
                    "description": "Leberfleck an der linken Schulter",
                    "report_uid": "f71adb87-4618-4e51-b9be-ad2976286cea",
                    "isMalign": False,
                    "examination": "Der Leberfleck sieht nicht weiter gefährlich aus",
                    "file": "https://storage.cloud.google.com/derma_pdfgen/finding-2.jpg"
                }
            ]
        }}
def process_finding_list(items: ReportModel):
    pass


class UpdateReportModel(BaseModel):
    name: Optional[str]
    completed: Optional[bool]

    class Config:
        schema_extra = {
            "example": {
                "name": "My important report",
                "completed": True,
            }
        }
