from fastapi import APIRouter, Body, Request, HTTPException, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from .models import ReportModel
from .service import ReportService

router = APIRouter()


@router.post("/", response_description="Add new report")
async def create_report(request: Request, report: ReportModel = Body(...)):
    gen_file_name = ReportService.create_pdf(report)
    report_link = ReportService.uploadToCloud(gen_file_name)
    ReportService.removeLocalPDF(gen_file_name)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=report_link)
