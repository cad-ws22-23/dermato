# PDF generation microservice

## Install

`python -mvenv env`

`env/Scripts/activate`

`pip install -r requirements.txt`

`python main.py`

### Credentials

Add your google credentials json as "google-derma.json"

### Docker

`docker build -t pdf .`

`docker run -d --name pdf -p 80:80 pdf`
