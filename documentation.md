# Project documentation

## Table of contents

    1 Meta data
      1.1 Project
      1.2 Maintainer
      1.3 Product/Software
      1.4 Project Structure
    2 Requirements
      2.1 Initial requirements
      2.2 Traceability matrix
    3 Application Overview
      3.1 Motivation
      3.2 Intended use
      3.3 Context
      3.3 Multitenancy concept and Tenant roles(theoretical)
    4 Technical requirements
      4.1 Cloud platform
      4.2 Multitenancy (technical implementation)
      4.3 Five Essentials of cloud service
      4.4 Twelve Factors (12f)
      4.5 Tech Stack
      4.6 Architecture
      4.7 WebUI
      4.8 API
      4.9 Implemented features
    5 DevOps
      5.1 Initial Deployment
      5.2 CI/CD
      5.3 Monitoring
    6 Security
    7 Commercial
      7.1 Cost Calculation
      7.2 Commercial Model
    8 Conclusion
    9 Declaration of Originality

## Image overview

    Image 1: Dermato tech stack
    Image 2: Terraform and helm
    Image 3: Architecture diagramm
    Image 4: Premium tenant architecture diagramm
    Image 5: Backend api
    Image 6: Pdf api
    Image 7: GCP monitor cpu
    Image 8: Monitor fa statistic
    Image 9: Fusion auth tenants
    Image 10: Fusion auth applications
    Image 11: Fusion auth edit applications
    Image 12: Fusion auth users
    Image 13: Fusion auth user roles

## Table overview

    Table 1: Maintainer
    Table 2: Productname and version
    Table 3: Kubernetes components
    Table 4: Microservices
    Table 5: Backend routes

## 1 Meta data

This chapter includes all meta-information about this project.

### 1.1 Project

This Cloud Native Project is a component of the "Cloud Application Development" module offered in the winter term of 2022/23 as part of the "Master of Informatics - Software Engineering" program at HTWG Konstanz. It is taught by Professor Dr. Eiglsperger.

### 1.2 Maintainer

The maintainer and creators of this project are:

| Name                    | Matriculation number |
| ----------------------- | -------------------- |
| Fabian Renz             | 294856               |
| Tobias Brenner          | 296799               |
| Maximilian Fletschinger | 295215               |

Table 1: Maintainer

### 1.3 Product/Software

The project includes all components of a cloud native application based on a microservice architecture. The aim of the productive software is to be deployed inside a Kubernetes cluster provided by one of the big three cloud providers: Google Cloud Platform (GCP), Amazon Web Services (AWS), or Microsoft Azure.

Due to the use of a monorepository, all components are included in this repository inside their own subdirectory. The components have their own product version.

The following table lists all the components, their version, and the reference to the subdirectory in the current repository.
The overall project name is **Deramto**.

| Product name        | Product version | Reference          |
| ------------------- | --------------- | ------------------ |
| Dermato frontend    | 1.0.0           | ./frontend         |
| Dermato backend     | 1.0.0           | ./backend          |
| Dermato auth-helper | 1.0.0           | ./auth/auth-server |
| Dermato pdf         | 1.0.0           | ./pdf              |

Table 2: Productname and version

### 1.4 Project structure

    .idea             -> code editor settings
    assets/           -> media and data for the documentation
    auth/
      auth-server/    -> authentification helper service for fusionauth
      fusionauth/     -> fusion auth set up
    backend/          -> backend fastapi service
    cloud/
      .terraform      -> terraform initial file
      docker/         -> mongo db local set up
      gcloud/
        docs/         -> documentation for the gke setup
        helm/         -> playground for helm - not in use
        templates/    -> yaml config files for gke components
      mongodb/        -> mongodb local playground Dockefile
      terraform/      -> terraform files for previous exercise
    frontend/         -> frontend reactjs service
    pdf/              -> pdf service
    .gitlab-ci.yml    -> gitlab ci config for ci/cd

## 2 Requirements

This chapter outlines the requirements for the project/software product.

### 2.1 Inital requirements

The requirements for the project have been defined in a separate document that was presented during the kickoff meeting on November 10th, 2022.

The original PDF file can be accessed through the provided link.

[requirements.pdf](./assets/requirements/requirements.pdf)

A converted Markdown equivalent is provided in the same directory:

[requirements.md](./assets/requirements/requirements.md)

The following documentation is structured based on the requirements file. Therefore, all the requirements are listed or included in the chapter of the table of contents. For each chapter, we will provide information on how we have implemented the features, infrastructure, and metrics.

### 2.2 Traceability matrix

The traceability matrix provides a comprehensive overview of the relationship between the requirements and the chapters in which they are fulfilled and the implementation is documented.

The matrix can be found in a separate file.

[traceability-matrix.md](./assets/requirements/traceability-matrix.md)

## 3 Application Overview

This chapter provides a comprehensive overview of the purpose use of the Deramto application.

### 3.1 Motivation

One of the challenges plaguing the German medical system for multiple years is the access to medical specialists (Fachärzte). Getting appointments can take multiple weeks or even months.
Especially with minor ailments the obstacle of arranging an appointment is often high enough to skip a visit at the doctor. This, in the long run, can lead to more severe ailments and is counterproductive.
In the case of dermatology a minor ailment could be a mole or skin lesion that could either be benign, or be a precursor of skin cancer.
Catching these malignant indicators might save the life of a patient, capacity to treat other patients and money in the healthcare system.
Furthermore, the digitalisation of modern medicine underway and especially younger patients may soon expect to book their appointments online, get their prescriptions by mail and communicate through digital platforms like ours.

### 3.2 Intended use

This application is designed to build an interface between medical providers and their patients.
The main functionality lies within the exchange of pictures, provided by the Patient, and a medical examination and report by a licensed medical professional.
Our focus is on the ease of use. Uploading a picture should not take more than a few minutes just as writing the report should be straight forward.
In this instance we specialised our application on dermatologists and dermatological institutes.
We focused our attention on indications like skin cancer, pressure sores and basic wound healing.

### 3.3 Context

In this context diagram is shown how our users interact with our application.

The main user groups are physicians (here doctor) and patients.

![context diagram](./assets/_media/context-diagram.png)

### 3.3 Multitenancy concept and Tenant roles(theoretical)

Related requirement:

> The application implements a multi-user, multi-tenant Software-as-a-Service (SaaS)

Our tenants consist of dermatologists, clinics and general practitioners (GP) with an additional specialisation in dermatology. In short, institutions connected with providing and delivering services within the dermatological medical field.  
Each tenant type has a different set of requirements, a different frequency of use and different budgets.

All these factors must be taken into account when creating a tenancy model.
As shown in the graphic below, we also have different sets of users within the institutions linked to our application.

![tenant-diagram](./assets/_media/dermato-tenants.png)

While a dermatological practice or clinic often has more than one physician tending to patient needs, a GP might only need access for one physician.

Additionally the user base of each tenant might vary in size, demography and frequency of use. The indications with which the patients present to an institution also directly affects the requirements of a tenant.

While an acute indication might only need one exchange between patient a physician, a chronic illness or prolonged wound healing adds additional and more frequent exchanges.
We created three different tiers of access to our application to provide our tenants with an optimised experience.

|                    | Free            | Basic           | Premium                 |
| ------------------ | --------------- | --------------- | ----------------------- |
| Customization      | No              | Yes             | Yes                     |
| Instances          | Shared instance | Shared instance | One instance per tenant |
| Report findings    | one per Day     | unlimited       | unlimited               |
| Create PDF reports | No              | Yes             | Yes                     |

While our basic and premium tier do not differ in functionality, a core difference is the deployment of a separate instance for each premium tenant. This ensures a high degree of data separation and an unrestricted access to our service.
Differences between the free tier and both paid tiers is the extent to which our service can be used. While a free tier tenant can only write one report per day, a basic or premium tenant has no restrictions in writing reports and can send these reports in PDF file format.
The ability to customize the frontend which users can see adds the possibility to bring in your own design and logo. Patients directly see the institution they interact with.

We recommend using our basic tier to tenants that use our service frequently but not daily and have a small number of requests per day.

The premium tier is aimed at larger institutions with multiple physicians working in parallel and process multiple requests per day.

The free tier is more or less a demo version to get a feel for our product and get an overview to which paid tier suits the tenants needs best.

More on the different tiers is found in chapter 7.

## 4 Technical requirements

This chapter focuses on the technical specifications of the application based on the requirements file from chapter 2.

### 4.1 Cloud platform

As a cloud provider, we decided to use the Google Cloud Platform (GCP).

We started with Amazon's AWS for Infrastructure as a Service (IaaS) functions, but when it came to utilizing Platform as a Service (PaaS), AWS became complicated and painful to run. We tried setting up the same services on GCP and found it to be much easier, which led us to switch to GCP for our project.

Since we do not have any experience with Microsoft Azure, it was not considered further in our decision.

### 4.2 Multitenancy (technical implementation)

In the "Application Overview" section, we only explained that our application, Dermato, is a multi-tenant software with different tenant roles. In this chapter, we aim to provide deeper insight into how the tenants are managed in Dermato. We will cover the process of tenant creation and the definition of roles.

It is crucial to mention that Fusion Auth is used to fulfill most of the requirements for multi-tenancy, including authentication and authorization. This was the main reason for using Fusion Auth.

![Fusion auth tenants](./assets/_media/multi-tenancy-tenants.png)

Image 9: Fusion auth tenants

Image 9 gives an overview of all tenants in the Fusion Auth admin console. All common CRUD (Create, Update, Delete) operations are available for each tenant, making it the main panel for tenant organization.

In Fusion Auth, an application must be configured and related to tenants. Ideally, only one application, Dermato, would need to be configured and all tenants assigned to it. However, Fusion Auth only allows for one tenant per application, which is confusing considering that it supports multi-tenancy. It is possible that this is a paid feature. To work around this issue, we create separate instances of both tenants and applications in Fusion Auth and configure the data in the same way. The only difference is that we update the tenant ID for each application.

![Fusion auth applications](./assets/_media/multi-tenancy-applications.png)

Image 10: Fusion auth applications

![Fusion auth edit applications](./assets/_media/multi-tenancy-edit-application.png)

Image 11: Fusion auth edit applications

Image 10 provides an overview of the applications, where all CRUD operations are possible. Image 11 displays the configuration of a single application. The 'Name', 'Authorized redirect URLs', and 'Logout URL' must be set in this section.

After the tenant and the corresponding application are set up, you can select the tenant when creating a user in the user section.

![Fusion auth users](./assets/_media/fusion-auth-users.png)

Image 12: Fusion auth users

Image 12 displays the management section of users associated with a specific tenant. It is possible for the same user to appear twice as they can belong to both tenants.

![Fusion auth user roles](./assets/_media/fusion-auth-user-roles.png)

Image 13: Fusion auth user roles

When editing a user, you can select the user's role. These roles are predefined when the application is added. The user's context is determined by their access to the patient or doctor views. There are two additional groups: freemium and custom. The freemium group is assigned to all doctors associated with a freemium tenant, which serves as the source for the application to determine that a doctor is limited to assigning only one report per day. The custom group gives doctors the ability to access the "settings" section, where they can customize the logo. This group is assigned to all doctors associated with a standard or premium tenant. Currently, role assignments are made manually, but the plan is to automate this process through a script that triggers a role change in the tenant. This can be achieved with the technical APIs provided by Fusion Auth.

### 4.3 Five Essentials of cloud service

For any cloud service, it plays a significant role to meet the 5 essentials of cloud service. These are Scalability and Flexibility, On-demand Self-Service, Broad Network Access, Resource Pooling and Rapid Elasticity. In the following, the implementation of the individual essentials is explained in more detail. In Dermato's case, these points are largely met by using the Google Kubernetes Engine (GKE) offered by Google Cloud Platforms (GCP).

**On-demand Self-Service:** This essential is achieved through the use of user-friendly web portals or APIs that allow users to quickly provision resources without the need for IT involvement. GKE provides an easy-to-use web interface and a powerful API for creating and managing containers and clusters, making it simple for users to provision and manage resources as needed.

**Broad Network Access:** This essential is achieved through the use of web-based applications and APIs, allowing access from any device with an internet connection. The app deployed on GKE can be accessed from anywhere with an internet connection, providing users with the ability to access and use the app from anywhere, at any time.

**Resource Pooling:** This essential is achieved through the use of virtualization and multi-tenant architectures, where resources are shared among multiple users or tenants. GKE uses Kubernetes to manage containers and clusters, allowing for the efficient allocation of resources and the sharing of resources among multiple tenants.

**Rapid Elasticity:** This essential is achieved through the use of auto-scaling and elastic computing, where resources are automatically provisioned and deprovisioned in response to changing demands. This allows for quick and efficient adjustments to meet changing business needs. GKE provides automatic scaling features through Kubernetes, allowing for rapid elasticity in response to changing demands.

**Measured Service:** This essential refers to the metering and billing of cloud resources based on usage, allowing for cost-effective consumption of resources based on actual demand. GKE provides detailed usage reports and billing information, making it simple for users to understand and manage their cloud resource usage and costs.

Overall, the app deployed on GKE takes advantage of these five essentials of cloud computing, providing users with a scalable, flexible, and cost-effective platform for deploying and managing cloud-based applications.

### 4.4 Twelve Factors (12f)

**Codebase:** The app codebase is managed using Git and GitLab for version control. This allows for easy collaboration and deployment of the project, which includes the frontend, pdf-service, backend cloud setup, and auth-service.

**Dependencies:** Dependencies are managed using npm for JavaScript and pip for Python, which makes it easy to manage the dependencies of the app.

**Configuration:** Most of the variables are stored in environment variables, but some values such as the database connection string are still hardcoded.

**Backing services:** The MongoDB is hosted separately on Atlas, which allows for easy management of the data and improved reliability.

**Build, release, run:** The app is integrated with CI/CD via GitLabCI, ensuring that the app is built, released, and run in an automated and repeatable way.

**Processes:** The frontend is stateless, while the backend logic is separated and the data storage is external. This allows for easy management and scalability.

**Port binding:** The app is exposed through a port binding implicitly due to the use of GKE services.

**Concurrency:** The frontend and backend can easily scale up and down as needed, as they are stateless components.

**Disposability:** The app has fast startup and shutdown capabilities through the use of GKE.

**Dev/prod parity:** The only difference between the development, staging, and production environments are the service links.

**Logs:** The logs are written to the standard output due to the use of GKE components.

**Administration processes:** So far, there hasn't been a real use case for standardized and repeatable administration processes, but they are available if needed in the future.

By following these 12 factors, the app can be optimized for the cloud, ensuring that it is scalable, flexible, and cost-effective. Additionally, by following these best practices, the Dermato-App can take advantage of the many benefits of cloud computing and provide a reliable and high-performing experience for users.

### 4.5 Tech Stack

![Dermato Tech Stack](./assets/_media/techstack/techstack.png)

Image 1: Dermato tech stack

The Dermato-App utilizes a modern tech stack to provide a fast, secure, and scalable platform for its users. The following technologies play a crucial role in delivering these features:

**ReactJS:** ReactJS is a JavaScript library for building user interfaces. It is a popular choice for frontend development due to its simplicity, ease of use, and ability to handle complex UI components. The use of ReactJS provides a responsive and user-friendly interface for the Dermato app.

**NodeJS-Express:** The NodeJS-Express Microservice is used to integrate Fusionauth into the app. Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications. The use of NodeJS and Express allows for fast and efficient processing of authorization and authentication requests.

**Python/FastAPI:** The backend and PDF-Microservice are written in Python using FastAPI. FastAPI is a modern, fast (high-performance) web framework for building APIs with Python 3.7+ based on standard Python type hints. The use of Python and FastAPI provides a fast and efficient way to handle complex backend logic and generate PDFs.

**MongoDB:** MongoDB is a document-based NoSQL database used to store app data. It provides a flexible and scalable solution for data storage that can easily handle the dynamic and growing needs of the Dermato app.

**Fusionauth:** Fusionauth is used for authorization and authentication in the Dermato app. It uses a Postgres database to store user data and provides a secure and efficient way to manage user accounts.

**Docker:** Docker is used to dockerize the different services in the Dermato app. This allows for easy deployment and management of the app components in a consistent environment.

**Kubernetes:** The Dermato app is deployed to Google Kubernetes Engine (GKE) using Kubernetes. This provides a powerful platform for managing and scaling the app components and ensures high availability and reliability.

**Google Cloud Platform:** The Dermato app is hosted on the Google Cloud Platform, which provides a scalable and reliable infrastructure for the app. The use of Google Cloud Platform provides the app with a secure and efficient platform for delivering its services.

**Git and Gitlab:** The Dermato app uses Git and Gitlab for version control. This provides an efficient and centralized way to manage the codebase and track changes over time.

In conclusion, the tech stack used in the Dermato app is designed to provide a fast, secure, and scalable platform for its users. The use of modern technologies such as ReactJS, NodeJS-Express, Python/FastAPI, MongoDB, and Fusionauth ensures that the app can handle complex and growing requirements with ease. The deployment of the app on Google Cloud Platform using Kubernetes and the use of Git and Gitlab for version control provide an efficient and reliable infrastructure for delivering the app's services.

![Terraform and Helm](./assets/_media/techstack/techstack-1.png)

Image 2: Terraform and helm

It remains to mention that Terraform and Helm would have been a great addition to that stack, but were not actively used in this project.
In the context of the project, Terraform and Helm would have helped to streamline the deployment and management of the different services and their dependencies , especially if additional services were to be added to the app. However, it is worth mentioning that the project was able to achieve the desired deployment and management through the definition of the relevant YAML files and the GitLab CI, showing that these tools are not always necessary for a successful project at this scale.

### 4.6 Architecture

This chapter provides a comprehensive understanding of the application architecture. It outlines all the different Kubernetes components and how they interact with one another.

To start with, we list all the Kubernetes components that will be utilized and give a description for each component:

| Kubernetes component | Description                                                                                                                                                                                                                                                                                                                                                                                                                        |
| -------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Ingress              | An Ingress component defines rules for routing HTTP traffic to applications running in a cluster.                                                                                                                                                                                                                                                                                                                                  |
| Deployment           | The deployment component informs Kubernetes on how to establish or alter instances of the containerized application, known as "pods" (see below)                                                                                                                                                                                                                                                                                   |
| Replica set          | The replica set is not a standalone component, but is still included in the following architecture diagram. It represents the number of deployed pods. These can be initially configured in the deployment component, but must not be static. This concept enables the creation and deletion of dynamic instances of a pod, which is the main concept of Kubernetes scalability.                                                   |
| Pod                  | A pod is the smallest unit within a Kubernetes cluster. It consists of one or more Linux containers that typically provide a single (but can provide multiple) Docker image.                                                                                                                                                                                                                                                       |
| Service              | The service component serves as a logical abstraction for a deployed group of pods, making it easier to access the pods without having to connect to a specific pod. When a pod is recreated, added, or deleted, it can result in a change of its internal IP, making direct access to the pods more complicated. The service component has a static name and port, which is accessible and routes requests to the available pods. |
| ConfigMap            | A Kubernetes ConfigMap is a simple API object that allows you to store configuration data for other Kubernetes components, such as pods.                                                                                                                                                                                                                                                                                           |

Table 3: Kubernetes components

After explaining all the used Kubernetes components, the following architecture diagram will provide a detailed view of how the different components are used and distributed. Furthermore, it will give insight on how the components communicate.

**Basic architecture**

![architecture-basic](./assets/_media/architecture-architecture-k8s.png)

Image 3: Architecture diagramm

First of all, we see in Image 3 the basic cloud architecture of the Dermato application. We will first focus on the components and their distribution based on the freemium and standard tenants, which all share the same architecture. The architecture for premium tenants will be shown in the next image.

In the architecture diagram, the first component at the top represents the client browser. Everyone who wants to interact with the application must use a proper web browser to access either the frontend or the Fusion Auth admin console. All common modern browsers are supported.

At the bottom, we see two separate Google Cloud concepts: the Google Artifact Registry and Google Bucket. The Google Artifact Registry is a newer concept that replaces the Google Container Registry. It is a single place for organizing all the container images and language packages, and it is used by Dermato as the location where the images of the separate microservices are stored. On the other side, there is the Google Bucket. The bucket represents a cloud storage for files and is used by Dermato to store images from patient findings and generated reports as PDF files.

The third component at the bottom is the application database. Although it is possible to integrate the database within the Kubernetes cluster and take advantage of its scalability, it can be more challenging to deploy it and ensure that all instances have the properly synchronized replicated data. In our case, using a stable deployed external database is still acceptable because the database only needs to pass data through without further calculations. For this purpose and with the benefits of a NoSQL database, the setup is suitable for a larger architecture up to a certain traffic load. The database in our case is provided outside the Google universe by the Atlas group, mainly because MongoDB by Atlas is more cost-effective.

Now we take a look at the large component in the middle. This is the Kubernetes zonal (europe-west-a) cluster, running on the Kubernetes Engine from Google Cloud Platform. Inside the cluster, there are six microservices that are all built similarly from an architectural viewpoint. This means that a deployed microservice mainly consists of pods provided by the Kubernetes deployment components. To access the microservice using a static name and IP, each microservice has its own service. Only the Fusion Auth and the Auth Helper microservice have an additional ConfigMap that provides some special configurations. To connect or access the cluster's microservices, Fusion Auth and the frontend, there are two Ingresses available. For more information on the purpose of each Kubernetes component, refer to Table 3 at the beginning of this chapter.

Let's now dive deeper into the use of microservices. The following table lists the microservices by their deployment name and gives information on their usage:

| Microservice        | Usage                                                                                                                                    |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| dermato-fe          | the frontend of the application which provides the web ui                                                                                |
| dermato-be          | the backend/business logic for the web ui                                                                                                |
| dermato-pdf         | creates a pdf report based on given report and finding data                                                                              |
| dermato-fusion-auh  | fusion auth instance that handles authorization and authentication                                                                       |
| dermato-fusion-db   | postgres database for the fusion auth instance                                                                                           |
| dermato-auth-helper | middleware between fusion auth and the frontend. Handles the routes and redirects for the fusion auth endpoints called from the frontend |

Table 4: Microservices

After the explanation of each microservice, we take a look at the communication between the services. Based on the usage of the microservices, the communication paths are no big surprise and are displayed in the architecture diagram with arrows between the individual components.

To make it clearer, let's examine this point by following a basic doctor user story. So when the doctor first accesses the application, he uses a client browser which sends a request to the IP address provided by the ingress. This redirects directly to the dermatology frontend microservice. Now, the doctor sees the landing page with the login options.

Next up, the doctor logs into the application. In the architecture view, it means that the dermato-fe frontend sends a request with the credentials to the dermato-auth-helper middleware microservice. The middleware takes care of validating the credentials and, if valid, getting an authentication token that keeps the frontend user logged in. So, now the middleware communicates with the dermato-fusion-auth microservice. The fusion auth instance needs to check the given credentials. To make this check, fusion auth needs to compare the given data to the credentials stored in the fusion auth database. Thus, the fusion auth is now communicating with the dermato-fusion-db database microservice. After validating that the credentials are correct, fusion auth gives back a token to the middleware, which makes a correct redirect to the doctor's dashboard view, passing the token through again.

The doctor's dashboard includes a list of the sent reports from the patients. To get this data, the dermato-fe frontend service requests and communicates with the dermato-be backend microservice. The backend just makes sure to pass back the right data and takes care of the calculations. So the actual data needed is stored in an external database. The backend now communicates with the atlas app-database, gets the data, and passes it back to the frontend.

When the doctor wants to view the detailed page of a report, there are features for viewing the findings images and the report as a PDF file. This data is stored in Google buckets, which the frontend microservices also need to request. So this is the final communication path from the frontend microservice.

When the doctor requests to create a PDF file, there is a backend route that triggers the PDF service creation. Therefore, the dermato-be microservice also has a communication path to the dermato-pdf microservice.

In total, we have many communications inside the Kubernetes cluster using the Kubernetes service components. Only the dermato-fe and the dermato-fusion-auth microservices have an interface to the outside world to access either the application frontend or the Fusion Auth admin console.

**Premium tenant architecure**

![architecture-tenant](./assets/_media/architecture-tenant-architecture-k8s.png)

Image 4: Premium tenant architecture diagramm

Image 4 now shows how the architecture changes for multiple premium tenants. The Kubernetes cluster with the microservices remains mainly the same as for the freemium and standard tenant roles. The only change will be in the data storage.

Each premium tenant has its own external application database and its own Google bucket for images and PDF files. This means that the premium tenants have their own isolated data storage, which is an advantage in terms of security and availability as no tenant data is included in their storage space.

### 4.7 WebUI

The interfaces that an application offers to the user to interact with the application is of highest relevance.
Therefore Dermato offers the user a modern dynamic frontend, which is implemented with ReactJS as well as a HTTP API.
The GUI of the application is explained in the following.

#### Login

![4.7-1 GUI-Landing Page](./assets/_media/frontend/landing-page.png)

Initially, a user has the opportunity to select the appropriate organization on the landing page. Thereupon the user is prompted with a login screen. Here he can log in with his username & password.

![4.7-2 GUI-Login](./assets/_media/frontend/login.png)

After a successful authentication the user will be logged in with his correct roles.
The roles that have a significant influence on the view are the doctor and patient roles.

#### Patient

A patient can first look at their previous reports. On screen 4.7.3, the status of the individual reports is highlighted by badges. This allows a patient to quickly see the status of his report.

![4.7-3 GUI-Patient1](./assets/_media/frontend/patient/patient-reports.png)

The patient can then also create a new report consisting of one or more findings. These findings consist of a title, a description and an image that represents the anomaly.

![4.7-4 GUI-Patient2](./assets/_media/frontend/patient/patient-findings.png)

After a report with the relevant findings has been created, it can be sent to the corresponding physician for evaluation.

![4.7-5 GUI-Patient3](./assets/_media/frontend/patient/patient-finding-detail.png)

After the doctor has given his opinion, the patient can then view this evaluation. Here the patient can see whether his finding is malignant or not and what exactly the doctor's examination is.

![4.7-6 GUI-Patient4](./assets/_media/frontend/patient/patient-review.png)

Lastly, the patient can also download the PDF copy of the examination.

![4.7-7 GUI-Patient4](./assets/_media/pdf/pdf.png)

#### Doctor

The doctor first sees a list of open reports that are ready for review. These are sorted according to the individual patients and can then be processed by the responsible doctor depending on their status.

![4.7-8 GUI-Doctor1](./assets/_media/frontend/doctor/doctor-list-open.png)

In addition, the doctor can filter by the status of the reports. In this way, he can also view completed reports that have already been processed.

![4.7-9 GUI-Doctor2](./assets/_media/frontend/doctor/doctor-list-closed.png)

Wenn ein Report "open for preview" ist kann der Arzt das Finding begutachten und einen Befund für den Report erstellen. Wenn er mit der Examination fertig ist, kann er dann auch ein PDF-Exemplar davon erstellen und und dem Patienten mitteilen, dass sein Report examiniert wurde

![4.7-10 GUI-Doctor3](./assets/_media/frontend/doctor/doctor-review.png)

Within an organization, the web interface can also be customized through the Admin role. Here, the user with the role intended for this can upload the logo of the company in the settings.

![4.7-11 GUI-Doctor4](./assets/_media/frontend/doctor/doctor-customization.png)

### 4.8 API

Related requirement:

> The application provides an API

The application Dermato is based on a microservice architecture. There are three microservices that implement and provide an API. The services are: the dermato-be backend service, the dermato-pdf PDF creation service, and the dermato-fusion-auth Fusion Auth instance. Since we used the third-party application Fusion Auth, we did not implement this API ourselves. Therefore, we will only take a detailed look at the other two services that were created by us.

**Dermato-be - backend service**

![backend api part1](./assets/_media/dermato-be-api1.png)
![backend api part2](./assets/_media/dermato-be-api2.png)
![backend api part3](./assets/_media/dermato-be-api3.png)
![backend api part4](./assets/_media/dermato-be-api4.png)

Image 5: Backend api

The backend service technology is FastAPI using Python. Due to this technology stack, there is an automatic inclusion of Swagger for the framework from FastAPI. Swagger is a tool for creating detailed documentation and a web UI for the implemented API. Image 5 is a screenshot of the Swagger UI showing the API interface from the dermato-be backend service.

The following table includes all the routes shown in the Swagger UI screen and a short description for each:

| Method | Path                          | Description                                                        |
| ------ | ----------------------------- | ------------------------------------------------------------------ |
| Post   | /dermaitem/upload             | Upload a single image; currently not in use; only for testing      |
| GET    | /dermaitem/                   | Listing all the existing dermaitems without filter                 |
| Post   | /dermaitem/                   | Create new item; includes the image upload                         |
| Post   | /dermaitem/findingsByReport   | List all findings from a specific report                           |
| GET    | /dermaitem/{id}               | Get a single dermaitem with given id                               |
| PUT    | /dermaitem/{id}               | Update a single dermaitem with given id                            |
| DELETE | /dermaitem/{id}               | Delete a single dermaitem with given id                            |
| ---    | ---                           | ---                                                                |
| GET    | /report/                      | List all existing reports                                          |
| POST   | /report/                      | Create a new Report with given data                                |
| POST   | /report/filterByUserAndTenant | Get all reports from given user and a given tenant                 |
| POST   | /report/reportsGroupByUser    | Get all reports grouped by user from specific tenant               |
| GET    | /report/{id}                  | Get a single report with given id                                  |
| PUT    | /report/{id}                  | Update a single report with given id                               |
| DELETE | /report/{id}                  | Delete a single report with given id                               |
| ---    | ---                           | ---                                                                |
| GET    | /tenant/                      | List all existing tenant entries                                   |
| POST   | /tenant/                      | Create a new tenant entry without file; only for tests; not active |
| POST   | /tenant/createWithFile        | Create a new tenant entry with file                                |
| GET    | /tenant/{id}                  | Get a single tenant with given id                                  |
| PUT    | /tenant/{id}                  | Update a single tenant with given id                               |
| DELETE | /tenant/{id}                  | Delete a single tenant with given id                               |
| GET    | /tenant/fa/{id}               | Get a single tenant with given fa_uid                              |
| PUT    | /tenant/logo/{id}             | Update a file from a existing tenant entry                         |

Table 5: Backend routes

As we can see in Table 5, there are three active models: dermaitem, report, and tenant.

The dermaitem model represents a finding from a patient, so it's important to assign the report UID, which is allocated to the user, and the additional information to the model.

The report model represents a single report. It is directly related to a specific user, which is connected by persistently storing the Fusion Auth user ID and the tenant ID. Additionally, every report has a state and an attribute for the link to a PDF file.

The tenant model represents the configuration for a specific tenant, so the tenant UID (fa_uid) needs to be set. The logo attribute stores a link to a custom logo if one is set.

Detailed schemas for each of the models are also given in the screenshot from the Swagger UI in Image 5.

**Dermato-pdf - pdf sevice**

![pdf api](./assets/_media/dermato-pdf-api.png)

Image 6: Pdf api

The dermato-pdf PDF service is also implemented with the FastAPI framework. Therefore, Swagger is also providing a detailed API documentation.

In this case, the API is much smaller due to the service only providing one function: PDF creation.
So there is only one route, which is a POST method with the path '/reports/'. This route creates a PDF file based on the given information and stores it in a Google bucket.

The information sent to the PDF service consists of a report and one or more findings for that report. Therefore, there are two defined models in this API: "Finding" and "Report." The two models are quite similar to the structure from the dermato-be backend service, so we will not go into detail here. The complete model schema is shown in Image 6 as well.

### 4.9 Implemented features

This chapter provides a brief overview of the features of the application. The focus of this chapter is fulfilling the requirement "The application contains on demand workloads (e.g. WEBUI interaction, API Calls) and long running asynchronous workloads (e.g. scheduled job to create report)".

Because most of the workflows have already been described in the preceding chapters, we will refer to them here instead of repeating.

The applications features are listed below:

- Authenticate to application / Login (patient / doctor)
- Create / Update / Delete Reports (patient / doctor)
- Filter Reports (doctor)
- Create / Update / Delete Findings including images (patient / doctor)
- Set / Modify Tenant Customization (doctor)
- Create PDF files from reports (doctor)
- Download PDF files

Nearly all of the features, excluding PDF creation, are on-demand workflows. The web UI interactions are listed in detail with screenshots in Chapter 4.7. The business logic and related APIs are the topic of Chapter 4.8. So jump into one of these chapters to get more detailed information on this.

Only the PDF creation feature has been implemented in our "long" asynchronous workflow. It runs in the background and notifies the user once the creation is finished. During testing with a maximum of three findings per report (3 pages in PDF), the service never took longer than 1 or 2 seconds, so it doesn't feel like a "long" asynchronous job. However, the creation time may increase with adding many findings.

## 5 DevOps

This chapter is all about DevOps and its implementation.

### 5.1 Initial Deployment

Setting up the whole application on the Google Cloud Platform is quite easy and takes less than five minutes using the defined YAML configuration files stored in the following directory of this repository:

`./docs/gcloud/templates`

In the same directory, there is a README file with a detailed step-by-step description. The following text provides an overview of how to deploy the application, but not to the same extent. For more details, please refer to the mentioned manual.

Note: the following commands assume that the Google Cloud CLI has been set up previously.

First, a new cluster must be created:

`gcloud container clusters create dermato-cluster --num-nodes=1 --zone=europe-west3-a`

Then navigate using a local console to the path where all the configuration files are stored.

`cd path/to/project/docs/gcloud/templates`

To deploy all Kubernetes components at once, you can run a single command that executes and deploys every single YAML configuration file recursively in this directory.

`kubectl apply -R -f .`

After executing this command, all the Kubernetes components are created and deployed in Google Kubernetes Engine. The two ingress components take a few minutes to set up. Now, the entire application is deployed.

There are still two minor updates that need to be done. As the project does not have a static IP or domain name, the frontend ingress IP and Fusion Auth IP need to be updated in the two ConfigMap components. To make the changes effective, a rollout restart command needs to be executed for the two deployments, including the changed ConfigMaps.

When the application is ready for productive use, you would typically order and pay for a specific domain name to access the application using this domain name. With a static domain name, the last two steps of updating the configmaps would not need to be done.

That's all. Now the application is accessible.

### 5.2 CI/CD

The most important actions for applying updates or new releases are done by using continuous integration and continuous deployment - short CI/CD.

Before explaining how this is done in our project, we will provide some information about the organization of the project. As you can read in Chapter 1.4, "Project Structure," we use a monorepo, meaning that every microservice is organized in a subdirectory of this project. For version control, we use GitLab, which makes build automation possible through configuration of the gitlab-ci file.

The gitlab-ci file is located by default in the root directory:

`./.gitlab-ci.yml`

In general, the gitlab-ci consists of one or multiple stages, each of which can be configured differently and isolated from the other stages.

We are now picking some lines from the gitlab-ci file to explain how the CI/CD is done and implemented for the dermato application.

As an example, we are now looking at the CI/CD for the frontend microservice from the Kubernetes cluster. Because the other stages for the other microservices are quite similar, it is enough to show the concept with one of the microservices.

The first stage for the frontend is called "push_frontend_image" and is responsible for pushing the frontend image to the Google Artifact Registry. The following code lines define when this stage should run.

```
only:
  changes:
    - frontend/**/*
  refs:
    - master
```

By default, it would run every time, but applying these filter options with the "only" field, the stage is only going to be executed if the merge is to the master branch and a change was made in the subdirectory for the frontend microservice. Merges on the master branch should already be tested changes as part of a new release of the software. The same stage could be made for the dev branch for deploying to a test environment, but currently, we do not have a test environment due to costs.

```
image: docker:latest
```

For the stage, the basic image used is the "docker:latest" image because it has all the Docker libraries installed.

```
before_script:
  # Login to Google Container Registry (https://europe-west3-docker.pkg.dev)  (europe-west3-docker.pkg.dev)
  - base64 -d $GCP_SA_KEY | docker login -u _json_key --password-stdin https://europe-west3-docker.pkg.dev
```

As the name suggests, the "before_script" options runs commands before the actual script begins. In our case, we need to log in to the Google Container Registry to be authorized to push an image later to the registry.

```
script:
  # Build and tag image for both GCR and Gitlab registries _
  - docker build -t europe-west3-docker.pkg.dev/$PROJECT_ID/$CI_PROJECT_NAMESPACE/dermato-fe:latest -t ./frontend
  # Push image to GCR
  - docker push europe-west3-docker.pkg.dev/$PROJECT_ID/$CI_PROJECT_NAMESPACE/dermato-fe:latest
```

Then the actual script is executed in the pipeline. First, the "docker build" command builds a new image with the name "dermato-fe:latest". The "latest" tag is important here. Why it is important will be explained in the next stage. Then, the next step is to push the image to the Google artifact registry.

So far, we have automated the creation of the Docker image and uploaded the image to the registry. The next stage is called "deploy_frontend" and rolls out the new release of the microservice.

```
dependencies:
  - frontend
```

The dependency option is to only let the stage run if the previous stage ran successfully, which only makes sense because we cannot rollout a new release if the new container failed to push to the registry.

```
image: google/cloud-sdk:latest
```

As the base image for this stage, we use the "google/cloud-sdk:latest" image because we need to execute some gcloud commands and all the necessary packages are installed in this image.

```
script:
  - base64 -d $GCP_SA_KEY > ${HOME}/gcloud-service-key.json
  - gcloud auth activate-service-account --key-file ${HOME}/gcloud-service-key.json
  - gcloud container clusters get-credentials $CLUSTER_NAME --zone $CLUSTER_ZONE --project $PROJECT_ID
  - kubectl rollout restart deployment/dermato-fe
```

In the actual script, the first three lines of code are responsible for authorizing the stage environment to execute commands directly inside the Google Kubernetes cluster. The authorization is made here using a Google IAM account. Now, the last command rolls out the new release of the microservice. Because in the Kubernetes deployment YAML file, the latest image is configured to be the base image of the Kubernetes pods, all pods are now going to be restarted and use the newer updated latest image from the Google Artifact Registry.

So now, after the second stage, we have automated the process where after a merge to the master branch, only the changed microservices will be containerized, pushed to a registry, and deployed with the new image inside the Kubernetes cluster.

After a longer experience to make this pipeline work, we can finally say that the same actions would be easier to do with Github as a version control system and Github Actions as the automation tool because there are better concepts for authorizing for the Google Cloud platform.

Another lesson learned is that using Helm and Terraform would be more intelligent here, as the infrastructure is not currently being version controlled in our implementation. The CI/CD process works as desired, but when a new release is released with an error, it is much more complicated to roll back to an older release in our case than if we were using Helm and Terraform for this.

### 5.3 Monitoring

To maintain the application during runtime, it is important to have a detailed monitoring of the memory and CPU usage or have an overview of the login and user.

Due to the current cloud architecture using CCP as a cloud provider and FusionAuth as a microservice handling authentication and authorization, there are quite a few monitoring features available out of the box.

GCP offers a Cloud Console to monitor all the deployed workloads:

![monitor gcp cpu](./assets/_media/monitor-gcp-cpu.png)

Image 7: GCP monitor cpu

In Image 7, you can see the information and charts that GCP provides regarding CPU usage. By switching to the Memory tab on the left side, the view changes and provides information and charts for memory usage. These two parameters make it easy to monitor if the load on the application is manageable. In our test cases, the load was not high enough to cause a critical state, but imagine if many people were interacting with the application it becomes more important to monitor CPU and memory usage. If the loads become too high, it may be necessary to modify the infrastructure.

To control and monitor logins, the Fusion Auth instance has its own monitoring system from which we can benefit. It appears as follows:

![monitor fa statistic](./assets/_media/monitor-fa-statistics.png)

Image 8: Monitor fa statistic

In Image 8, you can see that Fusion Auth provides a comprehensive overview of all tenants and the number of users and logins. In the left sidebar, there are also some other statistics: Login, Registration, Daily Active Users, and Monthly Active Users. For each statistic, there are detailed information and topics mostly displayed as charts.

To conclude, there are sufficient monitoring features available through the chosen cloud architecture to continuously control and monitor the software.

## 6 Security

Security is an important aspect of any software application and the Dermato App is no exception. In this project, several measures were taken to ensure the security of the app and its user`s data.

### Authorization and Authentication:

The Dermato App uses Fusionauth for authorization and authentication. An integration microservice was developed using NodeJS-Express to integrate with Fusionauth. This ensures that only authorized users have access to the app`s functionality and data.

### GCP IAM Service Accounts:

The Dermato App runs on Google Cloud Platform and security in context with GCP is ensured with IAM Service accounts. These service accounts are used to grant access to specific GCP resources and ensure that users only have access to the resources they need to perform their specific tasks.

### MongoDB Database Connection:

The Dermato App uses MongoDB to store its data. Currently, the database connection is established using a username and password. However, it is advisable to improve this in the future by not hardcoding the connection string and limiting the IP addresses that can connect to the database. This will enhance the security of the database and reduce the risk of unauthorized access.

### Backend Route Protection:

Currently, the backend routes of the Dermato App are not yet protected. The idea was to implement security using JWT (JSON Web Tokens) obtained from Fusionauth. However, due to limited time, this did not receive top priority. Nevertheless, it is highly recommended to implement security for the backend routes in future iterations of the project to ensure that the app's data and functionality are protected from unauthorized access.

In conclusion, the Dermato App has several security measures in place to protect its user`s data and functionality. While some areas can still be improved, the security measures currently in place are sufficient to ensure the app operates securely.

## 7 Commercial

This chapter gives an insigth abut the cost calculation and the commercial model

### 7.1 Cost Calculation

Our costs are split between:

- maintaining the infrastructure in gcloud and the connected microservices
- continuous development and integration of additional features
- Costs associated with future demands and requirements especially regarding legislation to come

To get a better understanding how our costs scale with use and workload of our tenants and users, we propose a pilot program with selected institutions to get a better understanding of demands and resulting traffic.

### 7.2 Commercial Model

Keeping in mind that our application is primarily used in the public healthcare sector we need to take a few things into consideration:

- The budget of smaller institutes like GP offices and dermatologist is limited.
- Competition prevails between medical institutions.
- Laws and policies need to be followed to get approval of our application.
- The less time is spent creating a diagnosis and report, the better.

Addressing the limited budget of smaller institutions. Our focus is not primarily on providing a platform to earn money for tenants. The usage of our application is meant to make it easier for a patient to get in contact with their physician.

By providing this interface we can control the flow of patients into the physicians' offices. A consultation for a benign mole needs resources like a receptionist to schedule an appointment, an office for the patient to wait in and the time of a physician.
Our tenants can screen for those type of cases inside our application, so they can save resources and provide a more pleasant experience for the patient.
Cases that need further treatment can be scheduled to come into the office and already have a basic medical history and a treatment plan could be prepared. Furthermore, providing better care for the patient.

By lowering the barrier to contact a physician, the case load can be increased, and more “valuable” cases can be treated.
During this process not much monetarization potential is given. The EBM (Eindeutiger Bemessungsmaßstab) provides the possibility to bill an initial patient contact fee for outpatient ("ambulant") treatment(see picture below).

![EBM 01320 ](./assets/_media/dermato-ebm.png)

By making it possible to review and report multiple cases in close succession, a potential to save time and therefore money arises.

Larger tenants like clinics and medical care centers gain additional resources and subsidies we can take advantage of to make our application more affordable.
The German government has passed the “Krankenhauszukunftsgesetz” (KHZG) which provides over 4 billion Euro with the goal of:

```
“… investing in their digital future - because we have just experienced during the pandemic how important well-equipped and functioning hospitals are.”
```

One essential goal is to strengthen and better the digital infrastructure in clinics. With this law in place an investment in our application can be reimbursed.
Therefore, charging an initial lump sum payment, with following monthly, yearly payments or licensing fees, is the best approach to equip clinics with our product.

Circling back to our tenant tiers we can assume the following:

- Our basic tier is more geared towards medical specialists (e.g., dermatologists).
- The premium tier could be additionally monetized by applying for grants under the KHZG.

One group of tenants not discussed here are GPs with a specialization in dermatology. While those physicians also can profit from our application, a monthly payment is not economically viable.
The smaller number of cases leads to a less desirable cost-benefit ratio. Here a pay-by-case approach could be more interesting.

## 8 Conclusion

In conclusion, the Dermato App project was a successful endeavor that allowed us to apply and understand various concepts and technologies discussed in the lecture. By implementing a remote communication platform for patients and doctors, we were able to showcase our ability to use the cloud platform, "Google Cloud Platform", and its related technologies such as Docker and Kubernetes.

Additionally, the implementation of key concepts such as the five essentials and the twelve factors of cloud applications allowed us to understand the importance of these factors in the development of a cloud-based application. Despite limited time, we were able to implement a functioning version of the Dermato App, however, some improvements could be made, such as better security measures for the backend API and some refactoring of the code.

We gained valuable experience and knowledge from this project, including an understanding of IaaS, PaaS, and SaaS, as well as the advantages of cloud computing. Furthermore, we learned about the big cloud providers such as AWS and GCP, and the benefits of using Kubernetes as a container orchestration tool.

In the future, if the Dermato App were to be further developed, it would be beneficial to implement Helm and Terraform to make it easier to manage the infrastructure and Kubernetes resources. Overall, despite the challenges faced during the project, it was a valuable learning experience that taught us the importance of cloud computing and its related technologies in today's fast-paced technological landscape.

## 9 Declaration of Originality

We, Fabian Renz, Tobias Brenner and Maximilian Fletschinger, hereby declare that the work submitted is entirely our own original work and has not been taken from any sources except where due reference has been made in the text.

We are fully aware of the consequences of plagiarism and understand that this declaration is an assurance that the work submitted is original.

Fabian Renz / 2023-02-05
Tobias Brenner / 2023-02-05
Maximilian Fletschinger / 2023-02-05
