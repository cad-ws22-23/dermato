import "./App.css";
import Frontend from "./layout/Frontend";
import Login from "./page/Login";
import Patient from "./page/Patient";
import Doctor from "./page/Doctor";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Reports from "./component/patient/Reports";
import Findings from "./component/patient/Findings";
import List from "./component/doctor/List";
import Review from "./component/doctor/Review";
import Error from "./page/Error";
import Settings from "./page/Settings";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<Frontend component={<Error />} />} />

          <Route path="/" element={<Login />} />
          <Route
            path="/patient/reports"
            element={
              <Frontend component={<Patient component={<Reports />} />} />
            }
          />
          <Route
            path="/patient/findings/:reportId"
            element={
              <Frontend component={<Patient component={<Findings />} />} />
            }
          />
          <Route
            path="/doctor/list"
            element={<Frontend component={<Doctor component={<List />} />} />}
          />
          <Route
            path="/doctor/review/:reportId"
            element={<Frontend component={<Doctor component={<Review />} />} />}
          />
          <Route
            path="/settings"
            element={<Frontend component={<Settings />} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
