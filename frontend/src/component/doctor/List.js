import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Card,
  Form,
  Row,
  Col,
  Container,
  ListGroup,
  Badge,
} from "react-bootstrap";
import { ChevronRight, CupHot, PersonBadgeFill } from "react-bootstrap-icons";
import config from "../../conf/config";

function List() {
  const [reportsByUser, setReportsByUser] = useState([]);
  const [reportsByUserCopy, setReportsByUserCopy] = useState([]);
  const [baseUri] = useState(process.env.REACT_APP_BACKEND_DOMAIN);
  const [user] = useState(
    config.demo.demoMode
      ? config.demo.demoUsers[config.demo.demoUser]
      : JSON.parse(localStorage.getItem("user"))
  );
  const [filter, setFilter] = useState("open");
  const [search, setSearch] = useState("");

  useEffect(() => {
    getDermaItems("open");
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getDermaItems = (filter) => {
    axios
      .post(baseUri + "/report/reportsGroupByUser", {
        type: filter,
        tenant_uid: user.tid,
      })
      .then((response) => {
        // handle success
        const reports_by_user = [];
        for (let key in response.data) {
          //reports_by_user[key] = response.data[key];
          reports_by_user.push(response.data[key]);
        }
        setReportsByUser(reports_by_user);
        setReportsByUserCopy(reports_by_user);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const changeFilter = (filter) => {
    setFilter(filter);
    getDermaItems(filter);
  };

  const onChangeSearch = (elem) => {
    let reportsByUserTmp = reportsByUserCopy.filter((item) =>
      item[0].user_email.toLowerCase().includes(elem.target.value.toLowerCase())
    );
    setSearch(elem.target.value);
    setReportsByUser(reportsByUserTmp);
  };

  return (
    <>
      <p className="m-0">
        Home &gt;&gt; <strong>List</strong>
      </p>
      <Card>
        <Card.Body style={{ background: "cadetblue" }}>
          <Container fluid>
            <Row>
              <Col>
                Filter Options:{" "}
                <span
                  onClick={() => changeFilter("open")}
                  style={{
                    textDecoration: "underline",
                    cursor: "pointer",
                    color: filter === "open" ? "darkblue" : "#0d6efd",
                  }}
                >
                  All open reports
                </span>
                ,{" "}
                <span
                  onClick={() => changeFilter("closed")}
                  style={{
                    textDecoration: "underline",
                    cursor: "pointer",
                    color: filter === "closed" ? "darkblue" : "#0d6efd",
                  }}
                >
                  Closed reports
                </span>
                ,{" "}
                <span
                  onClick={() => changeFilter("all")}
                  style={{
                    textDecoration: "underline",
                    cursor: "pointer",
                    color: filter === "all" ? "darkblue" : "#0d6efd",
                  }}
                >
                  All reports
                </span>
              </Col>
              <Col md={4}>
                <Form.Control
                  placeholder="Search patients"
                  value={search}
                  onChange={onChangeSearch}
                />
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </Card>
      {reportsByUser.map((reports, index) => {
        return (
          <Card key={index} className="my-3 bg-light">
            <Card.Body>
              <Card.Title className="mb-4 text-center">
                <PersonBadgeFill className={"me-2"} size={30} />
                Patient: {reports[0].user_email}
              </Card.Title>

              <ListGroup className="m-lg-5">
                {reports.map((report, index) => {
                  return (
                    <ListGroup.Item
                      action
                      href={"/doctor/review/" + report._id}
                      key={index}
                    >
                      <strong>{report.name}</strong>
                      {report.state === "waiting" && (
                        <Badge bg="secondary" className="ms-2">
                          Waiting for review
                        </Badge>
                      )}
                      {report.state === "available" && (
                        <Badge bg="danger" className="ms-2">
                          Closed
                        </Badge>
                      )}
                      <ChevronRight
                        aria-label="Basic example"
                        style={{ float: "right" }}
                        size={25}
                      />
                    </ListGroup.Item>
                  );
                })}
              </ListGroup>
            </Card.Body>
          </Card>
        );
      })}
      {reportsByUser.length === 0 && (
        <Card className="my-3 bg-light">
          <Card.Body className="text-center">
            <h5>Keine Patienten-Reports gefunden</h5>
            <p className="my-5">
              Zeit für eine Kaffeepause! <CupHot />
            </p>
          </Card.Body>
        </Card>
      )}
    </>
  );
}

export default List;
