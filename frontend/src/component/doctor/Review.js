import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import {
  Card,
  Button,
  Form,
  FormGroup,
  Accordion,
  Image,
  Row,
  Col,
  Container,
  Dropdown,
} from "react-bootstrap";
import { ChevronLeft, Search, XLg } from "react-bootstrap-icons";

function Review() {
  const [dermaItems, setDermaItems] = useState([]);
  const [dermaItemsCopy, setDermaItemsCopy] = useState([]);
  const [search, setSearch] = useState("");
  const [baseUri] = useState(process.env.REACT_APP_BACKEND_DOMAIN);
  const [baseUriPdf] = useState(process.env.REACT_APP_PDF_DOMAIN);
  const [report, setReport] = useState({});

  const { reportId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    getReport(reportId);
    getDermaItems(reportId);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getReport = (reportId) => {
    axios
      .get(baseUri + "/report/" + reportId)
      .then((response) => {
        // handle success
        setReport(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const getDermaItems = (reportId) => {
    axios
      .post(baseUri + "/dermaitem/findingsByReport", {
        report_uid: reportId,
      })
      .then((response) => {
        // handle success
        setDermaItems(response.data);
        setDermaItemsCopy(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const onChangeSearch = (elem) => {
    let dermaItems = dermaItemsCopy.filter((item) =>
      item.name.toLowerCase().includes(elem.target.value.toLowerCase())
    );
    setSearch(elem.target.value);
    setDermaItems(dermaItems);
  };

  const onChangeUpdateIsMalign = (id, elem) => {
    axios
      .put(baseUri + "/dermaitem/" + id, {
        isMalign: elem.target.value,
      })
      .then((response) => {
        // handle success
        getDermaItems(reportId);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const onChangeUpdateMedicalExamination = (id, elem) => {
    axios
      .put(baseUri + "/dermaitem/" + id, {
        examination: elem.target.value,
      })
      .then((response) => {
        // handle success
        getDermaItems(reportId);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const updateReportState = (state, goBack) => {
    axios
      .put(baseUri + "/report/" + reportId, {
        // state: elem.target.value,
        state: state,
      })
      .then((response) => {
        // handle success
        setReport(response.data);
        if (goBack) {
          navigate("/doctor/list");
        }
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const updateReportPdf = (pdfPath) => {
    axios
      .put(baseUri + "/report/" + reportId, {
        // state: elem.target.value,
        pdf: pdfPath,
      })
      .then((response) => {
        // handle success
        setReport(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const createPdf = () => {
    axios
      .post(baseUriPdf + "/reports/", {
        user: report.user_uid,
        name: report.name,
        items: dermaItems,
      })
      .then((response) => {
        // handle success
        updateReportPdf(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  return (
    <>
      <p className="m-0">
        Home &gt;&gt; List &gt;&gt; <strong>{report.name}</strong>
      </p>
      <Card className="mb-2">
        <Card.Body style={{ background: "cadetblue" }}>
          <Container fluid>
            <Row>
              <Col className="text-center d-md-none mb-3">
                <h5>{report.name}</h5>
              </Col>
            </Row>
            <Row className="mb-2">
              <Col>
                <Button variant="outline-dark" href="/doctor/list">
                  <ChevronLeft /> Back to reports
                </Button>
              </Col>
              <Col className="text-center d-none d-md-block">
                <h5>{report.name}</h5>
              </Col>
              <Col>
                {report.state === "waiting" && report.pdf === "" && (
                  <div style={{ float: "right" }}>
                    <Button
                      variant="primary"
                      onClick={createPdf}
                      className="mb-1 mb-lg-0"
                    >
                      Create Pdf
                    </Button>{" "}
                    <Button
                      variant="secondary"
                      onClick={() => alert("confirm")}
                      disabled
                      className="mb-1 mb-lg-0"
                    >
                      Show Pdf
                    </Button>{" "}
                    <Dropdown style={{ display: "inline" }}>
                      <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Send
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item
                          onClick={() => updateReportState("available", true)}
                        >
                          With pdf document
                        </Dropdown.Item>
                        <Dropdown.Item
                          onClick={() => updateReportState("available", true)}
                        >
                          Without pdf document
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                )}
                {report.state === "waiting" && report.pdf !== "" && (
                  <div style={{ float: "right" }}>
                    <Button
                      variant="primary"
                      onClick={createPdf}
                      className="mb-1 mb-lg-0"
                    >
                      Recreate Pdf
                    </Button>{" "}
                    <Button
                      variant="dark"
                      href={report.pdf}
                      target="_blank"
                      className="mb-1 mb-lg-0"
                    >
                      Show Pdf
                    </Button>{" "}
                    <Button
                      variant="success"
                      onClick={() => updateReportState("available", true)}
                    >
                      Send
                    </Button>
                  </div>
                )}
                {report.state === "available" && (
                  <div style={{ float: "right" }}>
                    <Button
                      variant="secondary"
                      disabled
                      onClick={() => alert("pdf")}
                      className="mb-1 mb-lg-0"
                    >
                      Show Pdf
                    </Button>{" "}
                    <Button
                      variant="danger"
                      onClick={() => updateReportState("waiting", false)}
                      className="mb-1 mb-lg-0"
                    >
                      Undo finish & edit
                      <XLg className="ms-2" />
                    </Button>{" "}
                  </div>
                )}
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </Card>
      <Card>
        <Card.Body>
          <Card.Title className="mb-4 text-center">
            <Search className={"me-2"} />
            Dermatology findings
          </Card.Title>

          <Row>
            <Col sm={{ span: 6, offset: 3 }} lg={{ span: 4, offset: 4 }}>
              <Form.Control
                className="text-center mb-4"
                placeholder="Search"
                value={search}
                onChange={onChangeSearch}
              />
            </Col>
          </Row>

          <Accordion>
            {dermaItems.map((dermaItem, index) => {
              return (
                <Accordion.Item
                  eventKey={index}
                  key={index}
                  style={{ backgroundColor: "aliceblue" }}
                >
                  <Accordion.Header>{dermaItem.name}</Accordion.Header>
                  <Accordion.Body>
                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Image</b>
                      </Form.Label>
                      <div className="text-center">
                        <Image
                          className="mt-2"
                          width="250"
                          height="250"
                          src={`${dermaItem.file}`}
                        />
                      </div>
                    </FormGroup>

                    <FormGroup>
                      <Form.Label>
                        <b>Title (from patient)</b>
                      </Form.Label>
                      <Form.Control
                        value={dermaItem.name ? dermaItem.name : ""}
                        disabled
                      />
                    </FormGroup>

                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Description (from patient)</b>
                      </Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={3}
                        value={
                          dermaItem.description ? dermaItem.description : ""
                        }
                        disabled
                      />
                    </FormGroup>

                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Malign</b>
                      </Form.Label>
                      <Form.Select
                        aria-label="Default select example"
                        value={dermaItem.isMalign}
                        disabled={report.state === "available"}
                        onChange={(elem) =>
                          onChangeUpdateIsMalign(dermaItem._id, elem)
                        }
                      >
                        <option value={true}>Ja</option>
                        <option value={false}>Nein</option>
                      </Form.Select>
                    </FormGroup>

                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Medical examination</b>
                      </Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={3}
                        disabled={report.state === "available"}
                        value={dermaItem.examination}
                        onChange={(elem) =>
                          onChangeUpdateMedicalExamination(dermaItem._id, elem)
                        }
                      />
                    </FormGroup>
                  </Accordion.Body>
                </Accordion.Item>
              );
            })}
          </Accordion>
        </Card.Body>
      </Card>
    </>
  );
}

export default Review;
