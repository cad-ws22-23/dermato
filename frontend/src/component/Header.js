import React, { useState } from "react";
import { Col, Dropdown, Image } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import DermatoLogo from "../image/dermato-logo.png";
import Man from "../image/man.jpg";
import config from "../conf/config";

function Header(props) {
  const [authBaseUri] = useState(process.env.REACT_APP_AUTH_HELPER_DOMAIN);
  const [user] = useState(
    config.demo.demoMode
      ? config.demo.demoUsers[config.demo.demoUser]
      : JSON.parse(localStorage.getItem("user"))
  );
  const [role] = useState(
    user.roles.includes("Patient") ? "Patient" : "Doctor"
  );

  const clearLocalStorage = () => {
    localStorage.clear();
  };

  return (
    <header>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Col>
            <Navbar.Brand href="/">
              <img
                src={DermatoLogo}
                width="130"
                height="40"
                className="d-inline-block align-top me-2"
                alt="Dermato logo"
              />
            </Navbar.Brand>
          </Col>
          <Col>
            <h5 className="text-center text-white">{role}</h5>
          </Col>
          <Col>
            <Dropdown style={{ float: "right", right: 0 }}>
              <Dropdown.Toggle variant="outline-light" id="dropdown-basic">
                <span className="d-none d-md-inline">{user.email}</span>
                <Image
                  roundedCircle
                  width={30}
                  height={30}
                  src={Man}
                  className={"ms-2 border border-dark"}
                />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item
                  href={
                    authBaseUri +
                    "/logout?tenant=d794664b-0234-454c-a634-c9ea4e9528e1"
                  }
                  onClick={clearLocalStorage}
                >
                  Logout
                </Dropdown.Item>
                {user.roles.includes("Custom") && (
                  <Dropdown.Item href="/settings">Settings</Dropdown.Item>
                )}
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Container>
      </Navbar>
    </header>
  );
}

export default Header;
