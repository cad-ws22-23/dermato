import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Card,
  ListGroup,
  Badge,
  Button,
  Container,
  Row,
  Col,
  Accordion,
} from "react-bootstrap";
import { ChevronRight, InfoCircle, Plus, Trash } from "react-bootstrap-icons";
import config from "../../conf/config";

function Reports() {
  const [baseUri] = useState(process.env.REACT_APP_BACKEND_DOMAIN);
  const [reports, setReports] = useState([]);
  const [user] = useState(
    config.demo.demoMode
      ? config.demo.demoUsers[config.demo.demoUser]
      : JSON.parse(localStorage.getItem("user"))
  );

  useEffect(() => {
    getReportsByUserAndTenant(user.sub, user.tid);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getReportsByUserAndTenant = (userUid, tenantUid) => {
    axios
      .post(baseUri + "/report/filterByUserAndTenant", {
        user_uid: userUid,
        tenant_uid: tenantUid,
      })
      .then((response) => {
        // handle success
        setReports(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const addReport = () => {
    axios
      .post(baseUri + "/report/", {
        name: "Report " + (reports.length + 1),
        user_uid: user.sub,
        user_email: user.email,
        tenant_uid: user.tid,
        state: "empty",
        pdf: "",
      })
      .then((response) => {
        // handle success
        getReportsByUserAndTenant(user.sub, user.tid);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const deleteReport = (elem, id) => {
    elem.preventDefault();
    axios
      .delete(baseUri + "/report/" + id)
      .then((response) => {
        // handle success
        getReportsByUserAndTenant(user.sub, user.tid);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  return (
    <>
      <p className="m-0">
        Home &gt;&gt; <strong>Reports</strong>
      </p>
      <Card className="mb-2">
        <Card.Body style={{ background: "cadetblue" }}>
          <Container fluid>
            <Row className="mb-2">
              <Col>
                <h5 className="text-center">All reports</h5>
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </Card>
      <Card style={{ minHeight: 400 }}>
        <Card.Body>
          <ListGroup className="m-lg-5">
            {reports.map((report, index) => {
              return (
                <ListGroup.Item
                  action
                  href={"/patient/findings/" + report._id}
                  key={index}
                >
                  <strong>{report.name}</strong>
                  {report.state === "empty" && (
                    <Badge bg="warning" className="ms-2">
                      Empty
                    </Badge>
                  )}
                  {report.state === "open" && (
                    <Badge bg="primary" className="ms-2">
                      Open
                    </Badge>
                  )}
                  {report.state === "waiting" && (
                    <Badge bg="secondary" className="ms-2">
                      Waiting for review
                    </Badge>
                  )}
                  {report.state === "available" && (
                    <Badge bg="success" className="ms-2">
                      Review available
                    </Badge>
                  )}
                  <ChevronRight
                    aria-label="Basic example"
                    style={{ float: "right" }}
                    size={25}
                  />
                  <Button
                    variant="outline-danger"
                    onClick={(elem) => deleteReport(elem, report._id)}
                    className="me-3 py-0"
                    style={{ float: "right" }}
                  >
                    <Trash />
                  </Button>
                </ListGroup.Item>
              );
            })}

            <ListGroup.Item
              style={{ background: "cadetblue" }}
              className={"text-center"}
              onClick={addReport}
              action
            >
              <Plus size={25} />
            </ListGroup.Item>
          </ListGroup>

          <Accordion className="m-lg-5">
            <Accordion.Item eventKey="0">
              <Accordion.Header>
                <strong>Badge legend</strong>
                <InfoCircle className="ms-2" />
              </Accordion.Header>
              <Accordion.Body>
                <p className="m-0">
                  <Badge bg="warning" className="me-2">
                    Empty
                  </Badge>
                  Report has no findings yet
                </p>
                <p className="m-0">
                  <Badge bg="primary" className="me-2">
                    Open
                  </Badge>
                  Report has findings, but not send yet
                </p>
                <p className="m-0">
                  <Badge bg="secondary" className="me-2">
                    Waiting for review
                  </Badge>
                  Report was sent. Waiting for doctor to review findings.
                </p>
                <p className="m-0">
                  <Badge bg="success" className="me-2">
                    Report available
                  </Badge>
                  Review for findings is availbale.
                </p>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Card.Body>
      </Card>
    </>
  );
}

export default Reports;
