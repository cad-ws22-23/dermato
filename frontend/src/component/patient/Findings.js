import React, { useState, useEffect } from "react";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import axios from "axios";
import {
  Card,
  Button,
  Form,
  FormGroup,
  Accordion,
  Image,
  Row,
  Col,
  Container,
} from "react-bootstrap";
import {
  ChevronLeft,
  Download,
  Search,
  Send,
  XLg,
} from "react-bootstrap-icons";

function Findings() {
  const [dermaItems, setDermaItems] = useState([]);
  const [dermaItemsCopy, setDermaItemsCopy] = useState([]);
  const [newItemName, setNewItemName] = useState("");
  const [newItemDescription, setNewItemDescription] = useState("");
  const [newItemImage, setNewItemImage] = useState(null);
  const [search, setSearch] = useState("");
  const [baseUri] = useState(process.env.REACT_APP_BACKEND_DOMAIN);
  const [report, setReport] = useState({});

  const { reportId } = useParams();
  const navigate = useNavigate();
  const { states } = useLocation();

  useEffect(() => {
    getReport(reportId);
    getDermaItems(reportId);
    console.log(states);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getReport = (reportId) => {
    axios
      .get(baseUri + "/report/" + reportId)
      .then((response) => {
        // handle success
        setReport(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const getDermaItems = (reportId) => {
    axios
      .post(baseUri + "/dermaitem/findingsByReport", {
        report_uid: reportId,
      })
      .then((response) => {
        // handle success
        setDermaItems(response.data);
        setDermaItemsCopy(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const saveNewDermaItem = (e) => {
    e.preventDefault();

    if (dermaItems.length === 0) {
      updateReportState("open", false);
    }

    let formData = new FormData();
    formData.append("name", newItemName);
    formData.append("description", newItemDescription);
    formData.append("report_uid", reportId);
    formData.append("isMalign", false);
    formData.append("examination", "");
    formData.append("file", newItemImage);
    axios
      .post(baseUri + "/dermaitem/", formData)
      .then((response) => {
        // handle success
        getDermaItems(reportId);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const onChangeUpdateName = (id, elem) => {
    axios
      .put(baseUri + "/dermaitem/" + id, {
        name: elem.target.value,
      })
      .then((response) => {
        // handle success
        getDermaItems(reportId);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const onChangeUpdateDescription = (id, elem) => {
    axios
      .put(baseUri + "/dermaitem/" + id, {
        description: elem.target.value,
      })
      .then((response) => {
        // handle success
        getDermaItems(reportId);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const onChangeSearch = (elem) => {
    let dermaItems = dermaItemsCopy.filter((item) =>
      item.name.toLowerCase().includes(elem.target.value.toLowerCase())
    );
    setSearch(elem.target.value);
    setDermaItems(dermaItems);
  };

  const deleteDermaItem = (id) => {
    if (dermaItems.length === 1) {
      updateReportState("empty", false);
    }

    axios
      .delete(baseUri + "/dermaitem/" + id)
      .then((response) => {
        // handle success
        getDermaItems(reportId);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const updateReportState = (state, goBack) => {
    axios
      .put(baseUri + "/report/" + reportId, {
        // state: elem.target.value,
        state: state,
      })
      .then((response) => {
        // handle success
        setReport(response.data);
        if (goBack) {
          navigate("/patient/reports");
        }
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  return (
    <>
      <p className="m-0">
        Home &gt;&gt; List &gt;&gt; <strong>{report.name}</strong>
      </p>
      <Card className="mb-2">
        <Card.Body style={{ background: "cadetblue" }}>
          <Container fluid>
            <Row className="mb-2">
              <Col>
                <Button variant="outline-dark" href="/patient/reports">
                  <ChevronLeft /> Back to reports
                </Button>
              </Col>
              <Col className="text-center">
                <h5>{report.name}</h5>
              </Col>
              <Col>
                {report.state === "open" && (
                  <Button
                    variant="success"
                    onClick={() => updateReportState("waiting", true)}
                    style={{ float: "right" }}
                  >
                    Send report for review
                    <Send className="ms-2" />
                  </Button>
                )}
                {report.state === "waiting" && (
                  <Button
                    variant="danger"
                    onClick={() => updateReportState("open", false)}
                    style={{ float: "right" }}
                  >
                    Cancel review & edit findings
                    <XLg className="ms-2" />
                  </Button>
                )}
                {report.state === "available" && (
                  <Button
                    variant={report.pdf === "" ? "outline-secondary" : "dark"}
                    href={report.pdf}
                    target="_blank"
                    disabled={report.pdf === ""}
                    style={{ float: "right" }}
                  >
                    Download Pdf
                    <Download className="ms-2" />
                  </Button>
                )}
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </Card>
      <Card
        style={{
          opacity: report.state === "waiting" ? 0.5 : 1,
        }}
      >
        <Card.Body>
          <Card.Title className="mb-4 text-center">
            <Search className={"me-2"} />
            Dermatology findings
          </Card.Title>

          <Row>
            <Col sm={{ span: 6, offset: 3 }} lg={{ span: 4, offset: 4 }}>
              <Form.Control
                className="text-center mb-4"
                placeholder="Search"
                disabled={report.state === "waiting"}
                value={search}
                onChange={onChangeSearch}
              />
            </Col>
          </Row>

          <Accordion>
            {dermaItems.map((dermaItem, index) => {
              return (
                <Accordion.Item
                  eventKey={index}
                  key={index}
                  style={{ backgroundColor: "aliceblue" }}
                >
                  <Accordion.Header>{dermaItem.name}</Accordion.Header>
                  <Accordion.Body>
                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Image</b>
                      </Form.Label>
                      <div className="text-center">
                        <Image
                          className="mt-2"
                          width="250"
                          height="250"
                          src={`${dermaItem.file}`}
                        />
                      </div>
                    </FormGroup>

                    <FormGroup>
                      <Form.Label>
                        <b>Title</b>
                      </Form.Label>
                      <Form.Control
                        value={dermaItem.name ? dermaItem.name : ""}
                        disabled={
                          report.state === "available" ||
                          report.state === "waiting"
                        }
                        onChange={(elem) =>
                          onChangeUpdateName(dermaItem._id, elem)
                        }
                      />
                    </FormGroup>

                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Description</b>
                      </Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={3}
                        disabled={
                          report.state === "available" ||
                          report.state === "waiting"
                        }
                        value={
                          dermaItem.description ? dermaItem.description : ""
                        }
                        onChange={(elem) =>
                          onChangeUpdateDescription(dermaItem._id, elem)
                        }
                      />
                    </FormGroup>

                    {report.state === "available" && (
                      <>
                        <FormGroup className="mt-2">
                          <Form.Label>
                            <b>Malign (from doctor)</b>
                          </Form.Label>
                          <Form.Select
                            aria-label="Default select example"
                            value={dermaItem.isMalign}
                            disabled
                          >
                            <option value={true}>Ja</option>
                            <option value={false}>Nein</option>
                          </Form.Select>
                        </FormGroup>

                        <FormGroup className="mt-2">
                          <Form.Label>
                            <b>Medical examination (from doctor)</b>
                          </Form.Label>
                          <Form.Control
                            as="textarea"
                            rows={3}
                            disabled
                            value={dermaItem.examination}
                          />
                        </FormGroup>
                      </>
                    )}

                    <div className="text-center mt-3">
                      <Button
                        variant="outline-danger"
                        disabled={report.state === "waiting"}
                        onClick={() => deleteDermaItem(dermaItem._id)}
                      >
                        Delete
                      </Button>
                    </div>
                  </Accordion.Body>
                </Accordion.Item>
              );
            })}
          </Accordion>

          {report.state !== "waiting" && report.state !== "available" && (
            <>
              <div className="text-center">
                <Form.Label className="mt-5">Add new finding</Form.Label>
              </div>

              <form onSubmit={saveNewDermaItem}>
                <Card style={{ backgroundColor: "whitesmoke" }}>
                  <Card.Body>
                    <FormGroup>
                      <Form.Label>
                        <b>Title *</b>
                      </Form.Label>
                      <Form.Control
                        value={newItemName}
                        onChange={(elem) => setNewItemName(elem.target.value)}
                      />
                    </FormGroup>

                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Description</b>
                      </Form.Label>
                      <Form.Control
                        as="textarea"
                        rows={3}
                        value={newItemDescription}
                        onChange={(elem) =>
                          setNewItemDescription(elem.target.value)
                        }
                      />
                    </FormGroup>

                    <FormGroup className="mt-2">
                      <Form.Label>
                        <b>Image *</b>
                      </Form.Label>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Control
                          type="file"
                          onChange={(elem) =>
                            setNewItemImage(elem.target.files[0])
                          }
                        />
                      </Form.Group>
                    </FormGroup>

                    <p style={{ float: "right" }}>* required</p>

                    <div className="text-center mt-4">
                      <Button
                        variant="success"
                        type="submit"
                        disabled={
                          newItemName === "" || newItemImage === undefined
                        }
                      >
                        Save
                      </Button>
                    </div>
                  </Card.Body>
                </Card>
              </form>
            </>
          )}
        </Card.Body>
      </Card>
    </>
  );
}

export default Findings;
