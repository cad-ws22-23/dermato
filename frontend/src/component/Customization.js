import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Card,
  Form,
  Row,
  Col,
  Container,
  FormGroup,
  Button,
} from "react-bootstrap";
import { ChevronLeft } from "react-bootstrap-icons";
import config from "../conf/config";

function Customization() {
  const [baseUri] = useState(process.env.REACT_APP_BACKEND_DOMAIN);
  const [user] = useState(
    config.demo.demoMode
      ? config.demo.demoUsers[config.demo.demoUser]
      : JSON.parse(localStorage.getItem("user"))
  );
  const [logo, setLogo] = useState(null);
  const [tenant, setTenant] = useState({});

  useEffect(() => {
    getOrCreateTenant();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const getOrCreateTenant = () => {
    axios
      .get(baseUri + "/tenant/fa/" + user.tid)
      .then((response) => {
        // handle success
        if (response.data) {
          setTenant(response.data);
        } else {
          createTenant();
        }
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const createTenant = () => {
    axios
      .post(baseUri + "/tenant/", {
        fa_uid: user.tid,
        name: "",
        license: "",
        logo: "",
      })
      .then((response) => {
        // handle success
        setTenant(response.data);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  const updateTenantLogo = (e) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("file", logo);
    axios
      .put(baseUri + "/tenant/logo/" + tenant._id, formData)
      .then((response) => {
        // handle success
        window.location.reload();
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  return (
    <>
      <p className="m-0">
        Home &gt;&gt; <strong>Settings</strong>
      </p>
      <Card>
        <Card.Body style={{ background: "cadetblue" }}>
          <Container fluid>
            <Row>
              <Col>
                <Col>
                  <Button variant="outline-dark" href="/doctor/list">
                    <ChevronLeft /> Back to reports
                  </Button>
                </Col>
              </Col>
              <Col>
                <h5 className="text-center">Settings</h5>
              </Col>
              <Col />
            </Row>
          </Container>
        </Card.Body>
      </Card>
      <Card className="my-3 bg-light">
        <Card.Body>
          <h5 className="text-center">Customization</h5>
          <form onSubmit={updateTenantLogo}>
            <FormGroup className="mt-2">
              <Form.Label>
                <b>Logo</b>
              </Form.Label>
              <Form.Group controlId="formFile" className="mb-3">
                <Form.Control
                  type="file"
                  onChange={(elem) => setLogo(elem.target.files[0])}
                />
              </Form.Group>
            </FormGroup>
            <div className="text-center mt-4">
              <Button
                variant="success"
                type="submit"
                disabled={logo === undefined}
              >
                Save
              </Button>
            </div>
          </form>
        </Card.Body>
      </Card>
    </>
  );
}

export default Customization;
