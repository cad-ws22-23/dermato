import Container from 'react-bootstrap/Container';

function Footer() {
  return (
    <footer>
        <Container fluid className='text-center'>
            <p>©Copyright Fabian Renz, Tobias Brenner, Maximilian Fletschinger</p>
        </Container>
    </footer>
  );
}

export default Footer;