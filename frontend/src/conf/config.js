module.exports = {
  //tenant based infos
  tenant: [
    {
      id: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
      name: "DRK",
      license: "standart",
    },
    {
      id: "d832e028-d661-4e2f-b9a8-0f50710d2f0b",
      name: "Marienkrankenhaus",
      license: "premium",
    },
    {
      id: "10d1a6d8-03e6-422d-b987-a4aeea7e81d6",
      name: "Hautarztpraxis Dr. Mustermann",
      license: "freemium",
    },
  ],
  demo: {
    demoMode: false,
    demoUser: 0,
    demoUsers: [
      {
        applicationId: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
        email: "john@doe.com",
        email_verified: true,
        roles: ["Patient"],
        sub: "1e5a73d3-a646-4216-a8c2-ee2c10489c1c",
        tid: "fd323-05-0607-0809-gtg3443",
      },
      {
        applicationId: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
        email: "jimmy@doe.com",
        email_verified: true,
        roles: ["Patient"],
        sub: "zhg4i3-4bi53-9gjg505gggt-gttt",
        tid: "fd323-05-0607-0809-gtg3443",
      },
      {
        applicationId: "c7ddeb7f-9d54-45a0-9f8b-97479720a20f",
        email: "jane@doe.com",
        email_verified: true,
        roles: ["Doctor", "Custom"],
        sub: "fewddfsdv4334-d07-0809-gtg3443",
        tid: "fd323-05-0607-0809-gtg3443",
      },
    ],
  },
};
