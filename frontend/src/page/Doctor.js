import React, { useState, useEffect } from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import config from "../conf/config";
import axios from "axios";

function Doctor({ component }) {
  const [user] = useState(
    config.demo.demoMode
      ? config.demo.demoUsers[config.demo.demoUser]
      : JSON.parse(localStorage.getItem("user"))
  );
  const [tenant, setTenant] = useState(null);
  const [baseUri] = useState(process.env.REACT_APP_BACKEND_DOMAIN);
  const [confLoaded, setConfLoaded] = useState(false);

  useEffect(() => {
    //check use and role
    if (user.email !== undefined && user.email_verified) {
      if (user.roles.includes("Doctor")) {
        getTenantConf();
      }
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  //check use and role
  if (user.email !== undefined && user.email_verified) {
    if (!user.roles.includes("Doctor")) {
      return <p className="text-center my-5">404 Permission denied</p>;
    }
  }

  const getTenantConf = () => {
    axios
      .get(baseUri + "/tenant/fa/" + user.tid)
      .then((response) => {
        // handle success
        if (response.data) {
          setTenant(response.data);
        }
        setConfLoaded(true);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };

  return (
    <Container className={"my-5"}>
      <Row>
        <Col className="text-center">
          {confLoaded && tenant !== null && (
            <Image
              alt="Company logo"
              width="130"
              height="60"
              src={`${tenant.logo}`}
            />
          )}
          {confLoaded && tenant === null && (
            <h4 style={{ height: 60 }}>Dermato App</h4>
          )}
          {!confLoaded && <div style={{ height: 60 }} />}
        </Col>
      </Row>
      <Row className="mt-2">
        <Col>{component}</Col>
      </Row>
    </Container>
  );
}

export default Doctor;
