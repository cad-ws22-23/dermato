import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import DermatoLogo from "../image/dermato-logo.png";
import "../style/login.css";
import config from "../conf/config";
import { useNavigate, useSearchParams } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";

function Login() {
  const [searchParams] = useSearchParams();
  const [tenant, setTenant] = useState("");
  const [valid, setValid] = useState(false);
  const [loading, setLoading] = useState(true);
  const [authBaseUri] = useState(process.env.REACT_APP_AUTH_HELPER_DOMAIN);
  const navigate = useNavigate();

  useEffect(() => {
    let loggedInUser = config.demo.demoMode
      ? config.demo.demoUsers[config.demo.demoUser]
      : JSON.parse(localStorage.getItem("user"));
    if (loggedInUser && Object.keys(loggedInUser).length > 0) {
      if (loggedInUser.roles.includes("Patient")) {
        navigate("/patient/reports", { state: loggedInUser });
      } else if (loggedInUser.roles.includes("Doctor")) {
        navigate("/doctor/list", { state: loggedInUser });
      }
    }

    if (searchParams.get("tenant")) {
      setTenant(searchParams.get("tenant"));
      fetch(authBaseUri + `/user?tenant=${searchParams.get("tenant")}`, {
        credentials: "include",
      })
        .then((response) => response.json())
        .then((data) => {
          //save user infos in localStorage
          localStorage.setItem("user", JSON.stringify(data));

          //navigate to right page
          if (data.email !== undefined && data.email_verified) {
            if (data.roles.includes("Patient")) {
              navigate("/patient/reports", { state: data });
            } else if (data.roles.includes("Doctor")) {
              navigate("/doctor/list", { state: data });
            } else {
              navigate("/");
            }
          }
          setLoading(false);
        });
    } else {
      setLoading(false);
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleSubmit = (event) => {
    event.preventDefault();

    if (tenant === "") {
      setValid(true);
    } else {
      window.location.href = authBaseUri + `/login?tenant=${tenant}`;
    }
  };

  return (
    <Container fluid>
      {loading ? (
        <Row style={{ height: "100vh", backgroundColor: "#212529" }}>
          <Col className="text-center">
            <Spinner
              animation="border"
              role="status"
              style={{
                color: "indianred",
                position: "fixed",
                top: 400,
              }}
            >
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </Col>
        </Row>
      ) : (
        <Row style={{ height: "100vh" }}>
          <Col md="6" className="login-background" style={{ minHeight: 70 }} />
          <Col md="6" style={{ backgroundColor: "#212529" }}>
            <Container className="form-ctn-margin-top">
              <Row className="text-center">
                <Col md={12}>
                  <img
                    src={DermatoLogo}
                    width="130"
                    height="40"
                    alt="React Bootstrap logo"
                  />
                  <h4 className="text-white my-5">Welcome to Dermato App</h4>
                </Col>
              </Row>
              <Row>
                <div className="offset-md-1 offset-lg-3 col-md-10 col-lg-6">
                  <form onSubmit={handleSubmit}>
                    <Form.Select
                      aria-label="Default select example"
                      className="mb-3 login"
                      onChange={(event) => setTenant(event.target.value)}
                    >
                      <option value="">Select Organization</option>
                      {config.tenant.map((tenant, index) => {
                        return (
                          <option value={tenant.id} key={index}>
                            {tenant.name}
                          </option>
                        );
                      })}
                    </Form.Select>
                    {valid && (
                      <p className={"text-center"} style={{ color: "red" }}>
                        Error: select tenant
                      </p>
                    )}

                    <div className="d-grid">
                      <Button variant="primary" type="submit">
                        Login
                      </Button>
                    </div>
                  </form>
                </div>
              </Row>
            </Container>
          </Col>
        </Row>
      )}
    </Container>
  );
}

export default Login;
