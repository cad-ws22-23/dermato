import { Container, Row, Card } from "react-bootstrap";

function Error() {
  return (
    <Container className={"my-5"}>
      <Row className="mt-4">
        <Card>
          <Card.Body className="my-5">
            <h5 className="text-center">404 Not found</h5>
            <p className="text-center">
              Sorry, the requested route was not found. Check again!
            </p>
          </Card.Body>
        </Card>
      </Row>
    </Container>
  );
}

export default Error;
