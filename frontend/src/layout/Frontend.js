import React from "react";
import Header from "../component/Header";
import Footer from "../component/Footer";
import { useLocation } from "react-router-dom";
import config from "../conf/config";

function Frontend({ component }) {
  const { state } = useLocation();

  if (!config.demo.demoMode) {
    if (localStorage.getItem("user") == null) {
      return <p>Error 403 permission denied</p>;
    }
  }

  return (
    <div className={"frontend"}>
      <Header user={state} />
      {component}
      <Footer />
    </div>
  );
}

export default Frontend;
